<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@include file="../adminHeader.jsp"%>

<!DOCTYPE html>
<html>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
<link rel="stylesheet" href="<%=request.getContextPath()%>/vistas/adminPage/estilosAdmin.css" type="text/css">
<head>
<meta charset="ISO-8859-1">

<title>Administrador</title>
</head>
<body>
	<section class="section">
		<div class="container">
			<div
				class="row md-m-25px-b m-45px-b justify-content-center text-center">
				<div class="col-lg-8">
					<h3 class="h1 m-15px-b">Tabla de control</h3>

				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-lg-4 m-15px-tb">
					<div
						class="media box-shadow-only-hover hover-top border-all-1 border-color-gray p-15px">
						<a class="overlay-link" href="<%= request.getContextPath()%>/vistas/arrendatarios/arrendatarios.jsp"></a>
						<div
							class="icon-50 theme-bg white-color border-radius-50 d-inline-block">
						</div>
						<div class="p-20px-l media-body">

							<h3 class="m-5px-tb">Arrendatarios</h3>
							<p class="m-0px font-small">Activos:10</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4 m-15px-tb">
					<div
						class="media box-shadow-only-hover hover-top border-all-1 border-color-gray p-15px">
						<a class="overlay-link" href="<%=request.getContextPath()%>/vistas/arrendadores/arrendadores.jsp"></a>
						<div
							class="icon-50 theme-bg white-color border-radius-50 d-inline-block">
						</div>
						<div class="p-20px-l media-body">

							<h3 class="m-5px-tb">Arrendadores</h3>
							<p class="m-0px font-small">Activos:23</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4 m-15px-tb">
					<div
						class="media box-shadow-only-hover hover-top border-all-1 border-color-gray p-15px">
						<a class="overlay-link" href="<%= request.getContextPath()%>/vistas/autos/autos.jsp"></a>
						<div
							class="icon-50 theme-bg white-color border-radius-50 d-inline-block">
						</div>
						<div class="p-20px-l media-body">

							<h3 class="m-5px-tb">Autos</h3>
							<p class="m-0px font-small">Activos: 4444</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4 m-15px-tb">
					<div
						class="media box-shadow-only-hover hover-top border-all-1 border-color-gray p-15px">
						<a class="overlay-link" href="<%= request.getContextPath()%>/vistas/casetas/casetas.jsp"></a>
						<div
							class="icon-50 theme-bg white-color border-radius-50 d-inline-block">
						</div>
						<div class="p-20px-l media-body">

							<h3 class="m-5px-tb">Casetas</h3>
							<p class="m-0px font-small">Activos:25</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


</body>
</html>