
<%@page import="models.Propiedades"%>
<%@page import="DAO.ArrendatarioDAO"%>
<%@page import="DAO.ArrendadorDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/vistas/autos/estilosAutos.css"
	type="text/css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<tr>
							<h5 class="card-title text-uppercase mb-0">Propiedad Arrendada de ""</h5>
					</div>
					<div class="table-responsive">
						<table class="table no-wrap user-table mb-0">
							<thead>
								<tr>
									<th scope="col"
										class="border-0 text-uppercase font-medium pl-4">#</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Numero Referencia</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Estatus</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Entradas</th>
								<th scope="col" class="border-0 text-uppercase font-medium">
										<form method="POST" action="<%=request.getContextPath()%>/vistas/propiedades/crearPropiedades.jsp?rfc=<%= request.getParameter("rfc")%>">
											<button type="submit" class="btn btn-primary ">+ Propiedades
											</button>
										</form>
									</th>
								</tr>
							</thead>
							
								<%
								
							Propiedades[] lista =null ;
							if(request.getParameter("tipo").equals("arrendatario")){
								ArrendatarioDAO arrendatarioDAO = new ArrendatarioDAO();
								//lista=arrendatarioDAO.buscarArrendatario(request.getParameter("rfc")).getPropiedades();
								//System.out.println(lista.length);	
									
								
							}else{
								ArrendadorDAO arrendadorDAO = new ArrendadorDAO();
								lista=arrendadorDAO.buscarArrendador(request.getParameter("rfc")).getPropiedades();
								System.out.println(lista.length);	
							}
								
							for(int i = 0; i < lista.length; i++){
							%>
							
							<tbody>
								<tr>
									<td class="pl-4">1</td>
									<td>
										<h5 class="font-medium mb-0"><%=lista[i].getNumeroReferencia() %></h5>
									</td>
									<td><span class="text-muted"><%=lista[i].getEstatusRenta()  %></span><br> </td>
									<td><span class="text-muted"><%=lista[i].getEntradas().length  %></span><br></td>
									<td>
									<th>
										<form method="POST" action="<%=request.getContextPath()%>/ArrendadorC?rfc=<%=request.getParameter("rfc")%>&numeroReferencia=<%=lista[i].getNumeroReferencia()%>">
											<button type="submit" name="borrarPropiedad"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-trash"></i>
											</button>
										</form>
									</th>
									<th>
										<form method="POST" action="<%=request.getContextPath()%>/vistas/propiedades/editarPropiedades.jsp?estatus=<%=lista[i].getEstatusRenta()  %>&descripcion=<%=lista[i].getDescripcion()%>&numeroReferencia=<%=lista[i].getNumeroReferencia()%>&calle=<%=lista[i].getDireccion().getCalle()%>&numero=<%=lista[i].getDireccion().getNumero()%>&zona=<%=lista[i].getDireccion().getZona()%>&cp=<%=lista[i].getDireccion().getCp()%>&rfc=<%= request.getParameter("rfc")%>">
											<button type="submit" name="editar"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-edit"></i>
											</button>
										</form>
									</th>
									</td>
								</tr>
							</tbody>
							
							<% }%>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>