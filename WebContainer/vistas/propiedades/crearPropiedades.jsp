<%@page import="org.apache.jasper.tagplugins.jstl.core.Catch"%>
<%@page import="models.Entrada"%>
<%@page import="models.Propiedades"%>
<%@page import="DAO.ArrendatarioDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="estilosPropiedades.css">
</head>

<body>
<h2>A�adir Propiedades</h2>
	<div class="container">
		<form method="POST" action="<%=request.getContextPath()%>/ArrendadorC?rfc=<%=request.getParameter("rfc")%>" class="was-validated">
			<div class="columna">
				<div class="form-group">
					<h2>Direccion</h2>
				</div>
				<div class="form-group">
					<label for="uname">Calle:</label> <input type="text"
						class="form-control" id="uname" placeholder="Ingrese la calle"
						name="calle" required>
					<div class="valid-feedback">Valid.</div>
				</div>

				<div class="form-group">
					<label for="uname">Numero:</label> <input type="text" title="Solo numeros"
						class="form-control" id="uname" placeholder="Ingrese el numero"
						 name="numero" required pattern="[0-9]+">
					<div class="valid-feedback">Valid.</div>
				</div>

				<div class="form-group">
					<label for="uname">Zona:</label> <input type="text"
						class="form-control" id="uname" placeholder="Ingrese la zona"
						 name="zona" required>
					<div class="valid-feedback">Valid.</div>
				</div>
								<div class="form-group">
					<label for="uname">CP:</label> <input type="text" title="Solo numeros, longitu de 5" minlength="5" maxlength="5"
						class="form-control" id="uname" placeholder="Ingrese el CP"
						 name="cp" required pattern="[0-9]+">
					<div class="valid-feedback">Valid.</div>
				</div>
			</div>
			
			<div class="columna">
			<div class="form-group">
					<h2>Datos</h2>
				</div>
			

				<div class="form-group">
					<label for="uname">Descripcion:</label> <input type="text"
						class="form-control" id="uname" placeholder="Ingrese la descripcion"
						 name="descripcion" required>
					<div class="valid-feedback">Valid.</div>
				</div>

				<div class="form-group">
					<label for="uname">Numero de Referencia:</label> <input type="text" title="Solo numeros , longitud de 5" minlength="5" maxlength="5"
						class="form-control" id="uname" placeholder="Ingrese el numero de referencia"
						 name="numeroReferencia" required pattern="[0-9]+">
					<div class="valid-feedback">Valid.</div>
				</div>
				
			</div>
			<div class="form-group">
					<button type="submit" name="crearPropiedad" class="btn btn-warning">Crear</button>
				</div>
			</form>
			</div>	
<br><br>
</body>
</html>
