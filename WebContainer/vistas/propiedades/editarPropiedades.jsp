<%@page import="DAO.ArrendadorDAO"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.Catch"%>
<%@page import="models.Entrada"%>
<%@page import="models.Propiedades"%>
<%@page import="DAO.ArrendatarioDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="estilosPropiedades.css">
</head>

<body>
<h2>Edicion de Propiedades</h2>
	<div class="container">
		<form method="POST" action="<%=request.getContextPath()%>/ArrendadorC?rfc=<%=request.getParameter("rfc")%>" class="was-validated">
			<div class="columna">
				<div class="form-group">
					<h2>Direccion</h2>
				</div>
				<div class="form-group">
					<label for="uname">Calle:</label> <input type="text" placeholder="Ingresa la calle"
						class="form-control" id="uname" value=<%=request.getParameter("calle")%>
						name="calle" required>
					<div class="valid-feedback">Valid.</div>
				</div>

				<div class="form-group">
					<label for="uname">Numero:</label> <input type="text" title="Solo numeros"
						class="form-control" id="uname" placeholder="Ingrese nombre"
						value=<%=request.getParameter("numero")%> name="numero" required pattern="[0-9]+">
					<div class="valid-feedback">Valid.</div>
				</div>

				<div class="form-group">
					<label for="uname">Zona:</label> <input type="text"
						class="form-control" id="uname" placeholder="Ingrese nombre"
						value=<%=request.getParameter("zona")%> name="zona" required>
					<div class="valid-feedback">Valid.</div>
				</div>
								<div class="form-group">
					<label for="uname">CP:</label> <input type="text" title="solo numeros, longitud de 5 " minlength="5" maxlength="5"
						class="form-control" id="uname" placeholder="Ingrese nombre"
						value=<%=request.getParameter("cp")%> name="cp" required pattern="[0-9]+" readonly="readonly">
					<div class="valid-feedback">Valid.</div>
				</div>
				<div class="form-group">
					<button type="submit" name="actualizarPropiedad" value="actualizarPropiedad" class="btn btn-warning">Actualizar</button>
				</div>
			</div>
			
			<div class="columna">
			<div class="form-group">
					<h2>Datos</h2>
				</div>
				<div class="form-group">
					<label for="uname">Estatus:</label> <input type="text"
						class="form-control" id="uname" value=<%=request.getParameter("estatus")%>
						name="estatus" required readonly="readonly">
					<div class="valid-feedback">Valid.</div>
				</div>

				<div class="form-group">
					<label for="uname">Descripcion:</label> <input type="text"
						class="form-control" id="uname" placeholder="Ingrese la descripcion"
						value=<%=request.getParameter("descripcion")%> name="descripcion" required>
					<div class="valid-feedback">Valid.</div>
				</div>

				<div class="form-group">
					<label for="uname">Numero de Referencia:</label> <input type="text" title="Solo numeros, longitud de 5" minlength="5" maxlength="5"
						class="form-control" id="uname" placeholder="Ingrese el numero de referencia"
						value=<%=request.getParameter("numeroReferencia")%> name="numeroReferencia" required pattern="[0-9]+" readonly="readonly">
					<div class="valid-feedback">Valid.</div>
				</div>
				
			</div>
			</form>
			</div>
		
		
		
		<div class="container">
		
		<form method="POST" action="<%=request.getContextPath()%>/ArrendadorC?rfc=<%=request.getParameter("rfc")%>&numeroReferencia=<%=request.getParameter("numeroReferencia")%>" class="was-validated">
			<div class="form-group">
					<label for="uname">A�adir nueva entrada :</label> <input type="text"
						class="form-control" id="uname" placeholder="Ingrese nombre" name="entrada" required>
					<div class="valid-feedback">Valid.</div>
				</div>
				<div class="form-group">
					<button type="submit" name="crearEntrada" class="btn btn-warning">A�adir</button>
				</div>
				
		</form>
		
		</div>
		<h2 align="center"><br>Entradas existentes</h2>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<tr>
							<h5 class="card-title text-uppercase mb-0">Casetas</h5>
					</div>
					<div class="table-responsive">
						<table class="table no-wrap user-table mb-0">
							<thead>
								<tr>
									<th scope="col"
										class="border-0 text-uppercase font-medium pl-4">#</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Nombre</th>
								</tr>
							</thead>
						
							<%ArrendadorDAO dao = new ArrendadorDAO();
								//poner conicion para cuando sea 0
								
							try{	
							for(Entrada entrada:dao.buscarPropiedad(request.getParameter("rfc"),request.getParameter("numeroReferencia")).getEntradas()){
						
						
							
							
							%>
							<tbody>
								<tr>
									<td class="pl-4"></td>
									<td>
										<h5 class="font-medium mb-0"><%=entrada.getNombre() %></h5>
									</td>
									
									

									<td>
									<th>
										<form method="POST" action=<%="" %>>
											<button type="submit" name="borrar"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-trash"></i>
											</button>
										</form>
									</th>
									<th>
									</th>
									</td>
								</tr>
						<% }
								
							}catch(Exception e){
								System.out.println("Error no hay propiedades disponibles");		
							
							} %>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
		
		
<br><br>
</body>
</html>
