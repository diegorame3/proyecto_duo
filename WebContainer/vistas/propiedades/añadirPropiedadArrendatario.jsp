<%@page import="java.util.ArrayList"%>
<%@page import="DAO.ArrendadorDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="estilosEditarArrendatarios.css">
</head>

<body>

	<div class="container">
		<h2>Aņadir propiedad a Arrendatario</h2>
		<br>
		<h3>Propiedades disponibles</h3>
		<form method="POST"
			action="<%=request.getContextPath()%>/ArrendatarioC?rfc=<%=request.getParameter("rfc") %>&tipo=hola"
			class="was-validated">


			<%
			//codigo para obtener todas las propiedades
			//consulta en arrendador
			//mostrar solo las propiedades que no esten rentadas
			ArrendadorDAO dao = new ArrendadorDAO();
			dao.mostrarPropiedadesDisponibles();
			
			
			
			%>
			<div class="form-group">
				<label>Seleccione Propiedad</label> <select name="categorias">
					<optgroup >
						<%--  insertar codigo para mandar a traer las categorias --%>

						<p>
							<% 
		            for (String[] item : dao.mostrarPropiedadesDisponibles()) {
		                   %><option> <%= "Propietario:  "+item[1]+"  _______  Clave Propiedad:"+item[0]%></option>
							<%}%>
						</p>
					</optgroup>
				</select>
			</div>




			<div class="form-group">

				<button type="submit" name="aņadirPropiedadRenta" 
					class="btn btn-warning">Agregar</button>
			</div>
	</div>

	</form>

	</div>
	<br>
	<br>
</body>
</html>
