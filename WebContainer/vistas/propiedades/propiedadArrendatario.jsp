
<%@page import="models.Propiedades"%>
<%@page import="DAO.ArrendatarioDAO"%>
<%@page import="DAO.ArrendadorDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/vistas/autos/estilosAutos.css"
	type="text/css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<tr>
							<h5 class="card-title text-uppercase mb-0">Propiedad Arrendada de ""</h5>
					</div>


					<%
					ArrendatarioDAO arrendatarioDAO = new ArrendatarioDAO();
					String lista = arrendatarioDAO.buscarArrendatario(request.getParameter("rfc")).getNumeroReferenciaPropiedad();
					%>

					<div class="table-responsive">
						<table class="table no-wrap user-table mb-0">
							<thead>
								<tr>
									<th scope="col"
										class="border-0 text-uppercase font-medium pl-4">#</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Numero Referencia</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Dueño</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Entradas</th>
								<th scope="col" class="border-0 text-uppercase font-medium">
										
										<%
										if(lista.equals("-1")){
										%>
										
										<form method="POST" action="<%=request.getContextPath()%>/vistas/propiedades/añadirPropiedadArrendatario.jsp?rfc=<%= request.getParameter("rfc")%>">
											<button type="submit"  class="btn btn-primary ">+ Propiedad
											</button>
										</form>
										<%}else{ %>
										<form method="POST" action="<%=request.getContextPath()%>/vistas/propiedades/añadirPropiedadArrendatario.jsp?rfc=<%= request.getParameter("rfc")%>">
											<button type="submit" disabled="disabled" class="btn btn-primary ">+ Propiedad
											</button>
										</form>
										
										
										<%} %>
									</th>
								</tr>
							</thead>
							
								
							<%
										if(!lista.equals("-1")){
										%>
							<tbody>
								<tr>
									<td class="pl-4">1</td>
									<td>
										<h5 class="font-medium mb-0">deeded</h5>
									</td>
									<td><span class="text-muted">deded</span><br> </td>
									<td><span class="text-muted"><%=lista  %></span><br></td>
									<td>
									<th>
										<form method="POST" action="<%=request.getContextPath()%>/ArrendatarioC?rfc=<%=request.getParameter("rfc") %>&identificador=<%=lista%>">
											<button type="submit" name="eliminaPropiedadRenta"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-trash"></i>
											</button>
										</form>
									</th>
									
									</td>
								</tr>
							</tbody>
							<%} %>
							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>