
<%@page import="models.Administrador"%>
<%@page import="DAO.AdministradorDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/vistas/administradores/estilosAdministradores.css"
	type="text/css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<tr>
							<h5 class="card-title text-uppercase mb-0">Administrador de
								administradores</h5>
					</div>
					<div class="table-responsive">
						<table class="table no-wrap user-table mb-0">
							<thead>
								<tr>
									<th scope="col"
										class="border-0 text-uppercase font-medium pl-4">#</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Nombre</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Correo</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Password</th>
<th scope="col" class="border-0 text-uppercase font-medium">
										<form action="<%=request.getContextPath()%>/vistas/administradores/crearAdministradores.jsp">
											<button type="submit" class="btn btn-primary ">+ Administrador
											</button>
										</form>
									</th>
								</tr>
							</thead>
							<tbody>
							
							<%AdministradorDAO dao = new AdministradorDAO(); %>
							<%for(Administrador administrador:dao.verAdministrador()){ %>
								<tr>
									<td class="pl-4">1</td>
									<td>
										<h5 class="font-medium mb-0"><%=administrador.getAlias() %></h5>
									</td>
									<td><span class="text-muted"><%=administrador.getCorreo() %></span><br> </td>
									<td><span class="text-muted"><%=administrador.getPassword() %></span><br></td>
									
									

									<td>
									<th>
										<form method="POST" action="<%=request.getContextPath()%>/Administrador?correo=<%=administrador.getCorreo()%>">
											<button type="submit" name="borrarAdministrador"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-trash"></i>
											</button>
										</form>
									</th>
									<th>
										<form method="POST" action="<%=request.getContextPath()%>/vistas/administradores/editarAdministradores.jsp?nombre=<%=administrador.getAlias() %>&correo=<%=administrador.getCorreo() %>&password=<%=administrador.getPassword() %>">
											<button type="submit" name="editar"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-edit"></i>
											</button>
										</form>
									</th>
									</td>
								</tr>
								
								<%} %>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>