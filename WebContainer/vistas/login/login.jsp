
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
<html>

<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/vistas/login/estilosLogin.css"
	media="screen" />
</head>

<body>
	<div class="sidenav">
		<div class="login-main-text">
			<h2>Zona Residencial</h2>
			<br> <br>
			<p>Inicia sesion para continuar</p>
		</div>
	</div>
	<div class="main">
		<div class="col-md-6 col-sm-12">
			<div class="login-form">
				<form method="POST" action="<%=request.getContextPath()%>/Administrador">
					<div class="form-group">
						<label>Correo Electronico</label> <input type="email"
							name="usuario" class="form-control" placeholder="Ingrese correo"
							required>
					</div>
					<div class="form-group">
						<label>Contraseña</label> <input type="password" name="contrasena"
							class="form-control" placeholder="Contrasena" required>
					</div>
					<button type="submit" name="inicioDeSesion" value="inicioDeSesion" i class="btn btn-black">Login</button>
			</div>
		</div>
	</div>
</body>
</html>
