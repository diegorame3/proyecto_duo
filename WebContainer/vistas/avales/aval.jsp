
<%@page import="models.Aval"%>
<%@page import="DAO.ArrendatarioDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/vistas/autos/estilosAutos.css"
	type="text/css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<tr>
							<h5 class="card-title text-uppercase mb-0">Avales de ""</h5>
					</div>
					<div class="table-responsive">
						<table class="table no-wrap user-table mb-0">
							<thead>
							
									<%
								
								Aval[] lista =null ;
							
								ArrendatarioDAO arrendatarioDAO = new ArrendatarioDAO();
								lista=arrendatarioDAO.buscarArrendatario(request.getParameter("rfc")).getAval();
								System.out.println(lista.length);	
								
							for(int i = 0; i < lista.length; i++){
							%>
							
								<tr>
									<th scope="col"
										class="border-0 text-uppercase font-medium pl-4">#</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Nombre</th>
									<th scope="col" class="border-0 text-uppercase font-medium">RFC</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Telefono</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Tipo Aval</th>
<th scope="col" class="border-0 text-uppercase font-medium">
										<form action="<%=request.getContextPath()%>/vistas/avales/crearAval.jsp">
											<button type="submit" class="btn btn-primary ">+ Aval
											</button>
										</form>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="pl-4">1</td>
									<td>
										<h5 class="font-medium mb-0"><%=lista[i].getNombre()  %></h5>
									</td>
									<td><span class="text-muted"><%=lista[i].getRFC()  %></span><br> </td>
									<td><span class="text-muted"><%=lista[i].getNumeroTelefonico()  %></span><br></td>
									<td><span class="text-muted"><%=lista[i].getTipoAval()  %></span><br></td>
									<td>
									<th>
										<form method="POST" action=<%="" %>>
											<button type="submit" name="borrar"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-trash"></i>
											</button>
										</form>
									</th>
									<th>
										<form method="POST" action="<%=request.getContextPath()%>/vistas/avales/editarAval.jsp?nombre=<%=lista[i].getNombre()%>&rfc=<%=lista[i].getRFC()%>&telefono=<%=lista[i].getNumeroTelefonico()%>&tipo=<%=lista[i].getTipoAval()%>">
											<button type="submit" name="editar"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-edit"></i>
											</button>
										</form>
									</th>
									</td>
								</tr>
							</tbody>
							<%}  %>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>