<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="estilosEditarArrendatarios.css">
</head>

<body>

	<div class="container">
		<h2>Edicion de Aval</h2>
		<br>
		<form method="POST" action="<%=request.getContextPath()%>/ArrendatarioC" class="was-validated">

			<div class="columna">
				<div class="form-group">
					<label for="uname">Nombre:</label> <input type="text"
						class="form-control" id="uname" 
						name="nombre" required>
					<div class="valid-feedback">Valid.</div>
				</div>

				<div class="form-group">
					<label for="uname">RFC:</label> <input type="text" minlength="13" maxlength="13"
						class="form-control" id="uname" placeholder="Ingrese nombre"
						 name="rfc" required>
					<div class="valid-feedback">Valid.</div>
					
				</div>

				<div class="form-group">
					<label for="uname">Telefono :</label> <input type="number" minlength="10" maxlength="10"
						class="form-control" id="uname" placeholder="Ingrese nombre"
						 name="telefono" required>
					<div class="valid-feedback">Valid.</div>
					
				</div>
				<div class="form-group">
					<label for="uname">Tipo de aval:</label> <input type="text"
						class="form-control" id="uname" placeholder="Ingrese nombre"
						 name="tipo" required>
					<div class="valid-feedback">Valid.</div>
					
				</div>
			</div>
			</div>
<br><br>
	<button type="submit" name="crearAval" value="actualizar" class="btn btn-warning">Crear</button>
			
			<br> <br> <br>
		</form>
		
	</div>
<br><br>
</body>
</html>
