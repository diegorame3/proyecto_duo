<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="estilosEditarArrendatarios.css">
</head>

<body>

	<div class="container">
		<h2>Edicion de Arrendatario</h2>
		<br>
		<h3>Informacion Personal</h3>
		<form method="POST" action="<%=request.getContextPath()%>/ArrendatarioC" class="was-validated">

			<div class="columna">
				<div class="form-group">
					<label for="uname">Apellido Materno:</label> <input type="text" title="Solo letras"
						class="form-control" id="uname" value=<%=request.getParameter("apellidoMat")%>
						name="apellidoMat" required  pattern="[A-Za-z]+">
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa el apellido.</div>
				</div>

				<div class="form-group">
					<label for="uname">Telefono :</label> <input type="text" minlength="10" maxlength="10"
						class="form-control" id="uname" placeholder="Ingrese nombre" title="Solo numeros, longitus de 10"
						value=<%=request.getParameter("numeroTelefonico")%> name="numeroTelefono" required pattern="[0-9]+">
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa el telefono.</div>
				</div>

				<div class="form-group">
					<label for="uname">Forma de pago :</label> <input type="text" title="Solo letras"
						class="form-control" id="uname" placeholder="Ingrese nombre"
						value=<%=request.getParameter("tipoPago")%> name="pago" required pattern="[A-Za-z]+">
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa la forma de
						pago.</div>
				</div>
			</div>

			<div class="columna">
				<div class="form-group">
					<label for="uname">Apellido Paterno:</label> <input type="text" title="Solo letras"
						class="form-control" id="uname" placeholder="Ingrese correo"
						value=<%=request.getParameter("apellidoPat")%> name="apellidoPat" required pattern="[A-Za-z]+">
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa el apellido.</div>
				</div>
				<div class="form-group">
					<label for="uname">CURP:</label> <input type="text" title="Solo mayusculas y numeros, lonitud de 18" minlength="18" maxlength="18"
						class="form-control" id="uname" placeholder="Ingrese telefono"
						value=<%=request.getParameter("curp")%> name="curp" required pattern="[A-Z0-9]+">
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa el curp.</div>
				</div>
				<div class="form-group">
					<label for="uname">Fecha Contrato:</label> <input type="date"
						class="form-control" id="uname" placeholder="Ingrese telefono"
						value=<%=request.getParameter("fechaContrato")%> name="fechaContrato" required>
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa la fecha.</div>
				</div>
			</div>

			<div class="columna">
				<div class="form-group">
					<label for="uname">Nombre:</label> <input type="text" title="Solo letras"
						class="form-control" id="uname" placeholder="Ingrese nombre"
						value=<%=request.getParameter("nombre")%>  name="nombre" required pattern="[A-Za-z]+">
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa el nombre.</div>
				</div>
				<div class="form-group">
					<label for="uname">RFC:</label> <input type="text" minlength="13" maxlength="13"
						class="form-control" id="uname" placeholder="Ingrese nombre"
						value=<%=request.getParameter("rfc")%> name="rfc" required  readonly="readonly">
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa el RFC.</div>
				</div>

				<div class="form-group">
					<label for="uname">Edad:</label> <input type="number" title="Solo numeros, mayor o igual a 18"
						class="form-control" id="uname" placeholder="Ingrese telefono" min="18"
						value=<%=request.getParameter("edad")%> name="edad" required pattern="[0-9]+" >
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa la edad.</div>
				</div>
			</div>
<br><br>
	<button type="submit" name="actualizar" value="actualizar" class="btn btn-warning">Actualizar</button>
			<button type="submit" name="verAutomovil" value="verAutomovil" class="btn btn-primary">Ver
				Automoviles</button>
			<button type="submit" name="verPropiedad" value="verPropiedad" class="btn btn-primary">Ver
				Propiedades</button>
			<button type="submit" name="verAval" class="btn btn-primary">Ver Aval</button>

			<br> <br> <br>
		</form>
		
	</div>
<br><br>
</body>
</html>
