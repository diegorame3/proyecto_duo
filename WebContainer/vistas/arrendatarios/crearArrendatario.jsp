<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="estilosEditarArrendatarios.css">
</head>

<body>

	<div class="container">
		<h2>Crear de Arrendatario</h2>
		<br>
		<h3>Informacion Personal</h3>
		<form method="POST"
			action="<%=request.getContextPath()%>/ArrendatarioC"
			class="was-validated">

			<div class="columna">
				<div class="form-group">
					<label for="uname">Apellido Materno:</label> <input type="text" title="Solo letras"
						class="form-control" id="uname"
						placeholder="Ingrese el apellido materno" name="apellidoMat"
						required  pattern="[A-Za-z]+">
					<div class="valid-feedback">Valid.</div>
				</div>

				<div class="form-group">
					<label for="uname">Telefono :</label> <input type="text"
						title="Solo numeros , 10 digitos" maxlength="10" minlength="10"
						class="form-control" id="uname" placeholder="Ingrese el telefono"
						name="numeroTelefono" required pattern="[0-9]*">
					<div class="valid-feedback">Valid.</div>
				</div>

				<div class="form-group">
					<label for="uname">Forma de pago :</label> <input type="text" title="Solo letras"
						class="form-control" id="uname"
						placeholder="Ingrese la forma de pago" name="pago" required pattern="[A-Za-z]+">
					<div class="valid-feedback">Valid.</div>
				</div>
			</div>

			<div class="columna">
				<div class="form-group">
					<label for="uname">Apellido Paterno:</label> <input type="text" title="Solo letras"
						class="form-control" id="uname"
						placeholder="Por favor ingresa el apellido paterno"
						name="apellidoPat" required pattern="[A-Za-z]+">
					<div class="valid-feedback">Valid.</div>

				</div>
				<div class="form-group">
					<label for="uname">CURP:</label> <input type="text"
						title="Solo mayusculas y numeros, lonitud de 18" minlength="18" maxlength="18"
						class="form-control" id="uname" placeholder="Ingrese su curp"
						name="curp" required pattern="[A-Z0-9]+">
					<div class="valid-feedback">Valid.</div>

				</div>
				<div class="form-group">
					<label for="uname">Fecha Contrato:</label> <input type="date" 
						class="form-control" id="uname"
						placeholder="Ingrese la fecha de contrato" name="fechaContrato"
						required>
					<div class="valid-feedback">Valid.</div>

				</div>
			</div>

			<div class="columna">
				<div class="form-group">
					<label for="uname">Nombre:</label> <input type="text" title="Solo letras"
						class="form-control" id="uname" placeholder="Ingrese nombre"
						name="nombre" required pattern="[A-Za-z]+">
					<div class="valid-feedback">Valid.</div>

				</div>
				<div class="form-group">
					<label for="uname">RFC:</label> <input type="text"
						title="Solo mayusculas o numeros, longitus de 13" minlength="13"
						maxlength="13" class="form-control" id="uname"
						placeholder="Ingrese RFC" name="rfc" required pattern="[A-Z0-9]+">
					<div class="valid-feedback">Valid.</div>

				</div>

				<div class="form-group">
					<label for="uname">Edad:</label> <input type="number"
						title="Solo se aceptan numeros myores a 18" class="form-control"
						id="uname" placeholder="Ingrese la edad" name="edad" min="18"
						required pattern="[0-9]*">
					<div class="valid-feedback">Valid.</div>

				</div>
			</div>
			<br>
			<br>
			<button type="submit" name="crearArrendatario" value="actualizar"
				class="btn btn-warning">Crear</button>
			<br> <br> <br>
		</form>

	</div>
	<br>
	<br>
</body>
</html>
