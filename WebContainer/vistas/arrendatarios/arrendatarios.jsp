
<%@page import="models.Arrendatario"%>
<%@page import="DAO.ArrendatarioDAO"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/vistas/arrendatarios/estilosArrendatarios.css"
	type="text/css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<tr>
							<h5 class="card-title text-uppercase mb-0">Administrador de
								arrendatarios</h5>
					</div>
					<div class="table-responsive">
						<table class="table no-wrap user-table mb-0">
							<thead>
								<tr>
									<th scope="col"
										class="border-0 text-uppercase font-medium pl-4">#</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Nombre</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Informacion</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Telefono</th>
									<th scope="col" class="border-0 text-uppercase font-medium"># Vehiculos</th>
									<th scope="col" class="border-0 text-uppercase font-medium"># Propiedad Asignada</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Fecha Contrato</th>
<th scope="col" class="border-0 text-uppercase font-medium">
										<form action="<%=request.getContextPath()%>/vistas/arrendatarios/crearArrendatario.jsp">
											<button type="submit" class="btn btn-primary ">+ Arrendatarios
											</button>
										</form>
									</th>

								</tr>
							</thead>
							<tbody>
							
							<% ArrendatarioDAO dao = new ArrendatarioDAO();%>
							<%  
							for(Arrendatario arrendatario:dao.verArrendatarios()){
								%>
							
							
								<tr>
									<td class="pl-4">1</td>
									<td>
										<h5 class="font-medium mb-0"><%=arrendatario.getApellidoPat()+"  "+arrendatario.getApellidoMat()  %></h5>
										<h5 class="font-medium mb-0"><%=arrendatario.getNombre()%></h5>
									</td>
									<td><span class="text-muted"><%=arrendatario.getCurp()%></span><br> <span
										class="text-muted"><%=arrendatario.getRFC()  %></span></td>
									<td><span class="text-muted"><%=arrendatario.getNumeroTelefono()  %></span><br></td>
									<td><span class="text-muted"> <%=arrendatario.getVehiculos().length  %></span></td>
									<td><span class="text-muted"> <%=arrendatario.getNumeroReferenciaPropiedad()  %></span></td>
									<td><span class="text-muted"><%=arrendatario.getFechaContrato()  %> </span><br>
									</td>

									<td>
									<th>
										<form method="POST" action=<%=request.getContextPath()+"/ArrendatarioC?rfc="+arrendatario.getRFC() %>>
											<button type="submit" name="borrar"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-trash"></i>
											</button>
										</form>
									</th>
									<th>
										<form method="POST" action="<%=request.getContextPath()%>/vistas/arrendatarios/editarArrendatarios.jsp?nombre=<%=arrendatario.getNombre()%>
										&apellidoPat=<%=arrendatario.getApellidoPat()%>&apellidoMat=<%=arrendatario.getApellidoMat()%>
										&curp=<%=arrendatario.getCurp()%>&rfc=<%=arrendatario.getRFC()%>&numeroTelefonico=<%=arrendatario.getNumeroTelefono()%>
										&vehiculo=<%=arrendatario.getVehiculos().length%>&propiedades=<%=arrendatario.getNumeroReferenciaPropiedad()%>&edad=<%=arrendatario.getEdad()%>&tipoPago=<%=arrendatario.getFormaPago()%>&fechaContrato=<%=arrendatario.getFechaContrato()%>">
											<button type="submit" name="editar"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-edit"></i>
											</button>
										</form>
									</th>
									</td>
								</tr>
								
								<%}  %>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>