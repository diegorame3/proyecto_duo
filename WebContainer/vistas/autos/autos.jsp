
<%@page import="java.util.ArrayList"%>
<%@page import="java.lang.reflect.Array"%>
<%@page import="DAO.ArrendatarioDAO"%>
<%@page import="models.Arrendatario"%>
<%@page import="models.Vehiculo"%>
<%@page import="models.Arrendador"%>
<%@page import="DAO.ArrendadorDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/vistas/autos/estilosAutos.css"
	type="text/css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<tr>
							<h5 class="card-title text-uppercase mb-0">Administrador de
								autos</h5>
					</div>
					<div class="table-responsive">
						<table class="table no-wrap user-table mb-0">
							<thead>
								<tr>
									<th scope="col"
										class="border-0 text-uppercase font-medium pl-4">#</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Matritula</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Tipo</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Marca</th>
									<th scope="col" class="border-0 text-uppercase font-medium">
										<form method="POST"
											action="<%=request.getContextPath()%>/vistas/autos/crearAutos.jsp?rfc=<%=request.getParameter("rfc")%>&tipo=<%=request.getParameter("tipo")%>">
											<button type="submit" class="btn btn-primary ">+
												Autos</button>
										</form>
									</th>
								</tr>
							</thead>

							<%
							System.out.println("tipo: "+request.getParameter("tipo")+"  rfc: "+request.getParameter("rfc"));
							Vehiculo[] lista =null ;
							if(request.getParameter("tipo").equals("arrendatario")){
								ArrendatarioDAO arrendatarioDAO = new ArrendatarioDAO();
								lista=arrendatarioDAO.buscarArrendatario(request.getParameter("rfc")).getVehiculos();
								System.out.println("Lista : "+lista.length);
									
								
							}else{
								ArrendadorDAO arrendadorDAO = new ArrendadorDAO();
								lista=arrendadorDAO.buscarArrendador(request.getParameter("rfc")).getVehiculos();
									
							}
								
							for(int i = 0; i < lista.length; i++){
							%>

							<tbody>
								<tr>
									<td class="pl-4"><%=i+1%></td>
									<td>
										<h5 class="font-medium mb-0"><%=lista[i].getMatricula()%></h5>
									</td>
									<td><span class="text-muted"><%=lista[i].getTipo()%></span><br>
									</td>
									<td><span class="text-muted"><%=lista[i].getMarca()%></span><br></td>



									<td>
									<th>
										<%
									if(request.getParameter("tipo").equals("arrendatario")){
										
									%>
										<form method="POST"
											action="<%=request.getContextPath()%>/ArrendatarioC?
										matricula=<%=lista[i].getMatricula()%>&rfc=<%=request.getParameter("rfc")%>">
											<button type="submit" name="borrarAutomovil"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-trash"></i>
											</button>
										</form> <%
									}else{
									%>

										<form method="POST"
											action="<%=request.getContextPath()%>/ArrendadorC?
									matricula=<%=lista[i].getMatricula()%>&rfc=<%=request.getParameter("rfc")%>">
											<button type="submit" name="borrarAutomovil"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-trash"></i>
											</button>
										</form> <%} %>

									</th>
									<th>
										<%
									if(request.getParameter("tipo").equals("arrendatario")){
										
									%>
										<form method="POST"
											action="<%=request.getContextPath()%>/vistas/autos/editarAutos.jsp
										?matricula=<%=lista[i].getMatricula()%>
										&tipo=<%=lista[i].getTipo()%>
										&tipoUsuario=arrendatario
										&marca=<%=lista[i].getMarca()%>
										&rfc=<%=request.getParameter("rfc")%>">
											<button type="submit" name="editarAutomovil"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-edit"></i>
											</button>
										</form> <%
									}else{
									%>
										<form method="POST"
											action="<%=request.getContextPath()%>/vistas/autos/editarAutos.jsp
										?matricula=<%=lista[i].getMatricula()%>
										&tipo=<%=lista[i].getTipo()%>
										&tipoUsuario=arrendador
										&marca=<%=lista[i].getMarca()%>
										&rfc=<%=request.getParameter("rfc")%>">
											<button type="submit" name="editarAutomovil"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-edit"></i>
											</button>
										</form> <%} %>

									</th>
									</td>
								</tr>

								<%
							}
								%>

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>