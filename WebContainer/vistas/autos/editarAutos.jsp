<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="estilosEditarArrendatarios.css">
</head>

<body>

	<div class="container">
		<h2>Edicion de Auto</h2>
		<br>
		<form method="POST"
			<%
		
		if(request.getParameter("tipoUsuario").equals("arrendador")){
			
			%>
			action="<%=request.getContextPath()%>/ArrendadorC?rfc=<%=request.getParameter("rfc")%>"
			class="was-validated">
			<% 
		}else{
			
		%>
			action="<%=request.getContextPath()%>/ArrendatarioC?rfc=<%=request.getParameter("rfc")%>"
			class="was-validated">


			<%
		}
		%>

			<div class="columna">
				<div class="form-group">
					<label for="uname">Matricula:</label> <input type="text" title="Solo mayusculas y numeros, lonitud de 7"
						minlength="7" maxlength="7" class="form-control" id="uname"
						value=<%=request.getParameter("matricula")%> name="matricula"
						required  pattern="[A-Z0-9]+">
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa la matricula.</div>
				</div>

				<div class="form-group">
					<label for="uname">Tipo:</label> <input type="text" title="Solo letras"
						class="form-control" id="uname" placeholder="Ingrese nombre"
						value=<%=request.getParameter("tipo")%> name="tipo" required pattern="[A-Za-z]+">
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa el tipo de
						auto.</div>
				</div>

				<div class="form-group">
					<label for="uname">Marca :</label> <input type="text" title="Solo letras"
						class="form-control" id="uname" placeholder="Ingrese nombre"
						value=<%=request.getParameter("marca")%> name="marca" required pattern="[A-Za-z]+">
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa la marca.</div>
				</div>
			</div>
			<br> <br>
			<button type="submit" name="actualizarAutomovil"
				class="btn btn-warning">Actualizar</button>

			<br> <br> <br>
		</form>

	</div>
	<br>
	<br>
</body>
</html>
