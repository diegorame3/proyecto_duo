
<%@page import="models.Arrendador"%>
<%@page import="org.bson.Document"%>
<%@page import="com.mongodb.client.FindIterable"%>
<%@page import="DAO.ArrendadorDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/vistas/arrendadores/estilosArrendadores.css"
	type="text/css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<tr>
							<h5 class="card-title text-uppercase mb-0">Administrador de
								arrendadores</h5>
					</div>
					<div class="table-responsive">
						<table class="table no-wrap user-table mb-0">
							<thead>
								<tr>
									<th scope="col" class="border-0 text-uppercase font-medium">Nombre</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Informacion</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Telefono</th>
									<th scope="col" class="border-0 text-uppercase font-medium">#
										Vehiculos</th>
									<th scope="col" class="border-0 text-uppercase font-medium">#
										Propiedades</th>
									<th scope="col" class="border-0 text-uppercase font-medium">
										<form action="<%=request.getContextPath()%>/vistas/arrendadores/crearArrendadores.jsp">
											<button type="submit"   class="btn btn-primary ">+ Arrendador
											</button>
										</form>
									</th>

								</tr>
							</thead>

							<%
							ArrendadorDAO arrendadorDAO = new ArrendadorDAO();
							for(Arrendador lista : arrendadorDAO.verArrendador()) {
							%>
							<tbody>
								<tr>
									<td>
										<h5 class="font-medium mb-0"><%=lista.getNombre()%></h5>
										<h5 class="font-medium mb-0"><%=lista.getApellidoPat()%> <%=lista.getApellidoMat()%></h5>
									</td>
									<td>
										<span class="text-muted">Curp: <%=lista.getCurp()%></span>
										<br> 
										<span
										class="text-muted">RFC: <%=lista.getRFC()%></span>
										<br> 
										<span class="text-muted">Edad: <%=lista.getEdad()%></span>
									</td>
									<td><span class="text-muted">Telefono: <%=lista.getNumeroTelefonico()%></span><br></td>
									<td><span class="text-muted"><%=lista.getVehiculos().length%></span></td>
									<td><span class="text-muted"><%=lista.getPropiedades().length%></span></td>
								

<!-- 									<td> -->
									<th>
										<form method="POST" action="<%=request.getContextPath()%>/ArrendadorC?rfc=<%=lista.getRFC()%>">
											<button type="submit" name="borrarArrendador" 
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-0">
												<i class="fa fa-trash"></i>
											</button>
										</form>
									</th>
									<th>
										<form method="POST" action="<%=request.getContextPath()%>/vistas/arrendadores/editarArrendadores.jsp?nombre=<%=lista.getNombre()%>
										&apellidoPat=<%=lista.getApellidoPat()%>&apellidoMat=<%=lista.getApellidoMat()%>
										&curp=<%=lista.getCurp()%>&rfc=<%=lista.getRFC()%>&numeroTelefonico=<%=lista.getNumeroTelefonico()%>
										&vehiculo=<%=lista.getVehiculos().length%>&propiedades=<%=lista.getPropiedades().length%>&edad=<%=lista.getEdad()%>">
											<button type="submit" name="editar"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-0">
												<i class="fa fa-edit"></i>
											</button>
										</form>
									</th>
								
								</tr>
								<%
		}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>