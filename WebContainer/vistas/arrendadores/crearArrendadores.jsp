<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/vistas/arrendadores/estilosEditarArrendadores.css">
</head>

<body>

	<div class="container">
		<h2>A�ade  Arrendador</h2>
		<br>
		<h3>Informacion Personal</h3>
		<form method="POST" action="<%=request.getContextPath()%>/ArrendadorC" class="was-validated">

			<div class="columna">
				<div class="form-group">
					<label for="uname">Apellido Materno:</label> <input type="text" title="Solo letras"
						class="form-control" id="uname"
						 name="apellidoMat"
						required pattern="[A-Za-z]+">
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa el apellido
						materno.</div>
				</div>

				<div class="form-group">
					<label for="uname">Telefono :</label> <input type="text" title="Solo numeros longitud de 10"
						class="form-control" id="uname" maxlength="10" minlength="10"
						
						name="numeroTelefonico" required pattern="[0-9]+">
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa el telefono.</div>
				</div>
			</div>

			<div class="columna">
				<div class="form-group">
					<label for="uname">Apellido Paterno:</label> <input type="text" title="Solo letras"
						class="form-control" id="uname"
						 name="apellidoPat"
						required pattern="[A-Za-z]+">
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa el apellido
						paterno.</div>
				</div>
				<div class="form-group">
					<label for="uname">CURP:</label> <input type="text" title="Solo mayusculas y numeros, lonitud de 18" minlength="18" maxlength="18"
						class="form-control" id="uname"
						 name="curp" required pattern="[A-Z0-9]+">
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa el curp.</div>
				</div>
			</div>

			<div class="columna">
				<div class="form-group">
					<label for="uname">Nombre:</label> <input type="text" title="Solo letras"
						class="form-control" id="uname"
						 name="nombre" required  pattern="[A-Za-z]+">
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa el nombre.</div>
				</div>
				<div class="form-group">
					<label for="uname">RFC:</label> <input type="text" title="Solo mayusculas o numeros, longitus de 13" minlength="13" maxlength="13"
						class="form-control" id="uname"
						 name="rfc" required pattern="[A-Z0-9]+">
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa el RFC.</div>
				</div>

				<div class="form-group">
					<label for="uname">Edad:</label> <input type="number"
						class="form-control" id="uname"
						 name="edad" required>
					<div class="valid-feedback">Valid.</div>
					<div class="invalid-feedback">Por favor ingresa la edad.</div>
				</div>
			</div>
			<br>
			<br>
			<button type="submit" name="crearArrendatario"  class="btn btn-success">Crear Arrendador</button>
			<br> <br> <br>
		</form>
	</div>
	<br>
	<br>
</body>
</html>
