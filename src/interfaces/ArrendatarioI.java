package interfaces;

import java.util.ArrayList;

import org.bson.Document;

import com.mongodb.client.FindIterable;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

import models.Arrendatario;
import models.Aval;
import models.Propiedades;
import models.Vehiculo;

public interface ArrendatarioI {
	
	public ArrayList<Arrendatario> verArrendatarios();
	
	public Arrendatario buscarArrendatario(String rfc);
	public Arrendatario buscarPropiedad(String rfc);
	public void crearArrendatario(String nombre, String apellidoPat, String apellidoMat, String curp,String edad, String numeroTelefonico,String rfc, Vehiculo []vehiculos, String numeroReferenciaPropiedad,Aval[] aval,String pago,String fechaContrato);
	public DeleteResult borrarArrendatario(String rfc);
	
	public  UpdateResult actualizarArrendatario(String rfc,String nombre,String apellidoMat,String apellidoPat,String numeroTelefono,String curp,String edad);
	public  UpdateResult actualizarArrendatario(String rfc,String numeroReferenciaPropiedad);
	
}
