package interfaces;

import java.util.ArrayList;

import org.bson.conversions.Bson;

import models.Arrendador;
import models.Arrendatario;
import models.Propiedades;
import models.Vehiculo;

public interface ArrendadorI {
	public ArrayList<Arrendador> verArrendador();
	
	public Arrendador buscarArrendador(String rfc);
	
	public void crearArrendador(String nombre, String apellidoPat, String apellidoMat, String curp,String edad, String numeroTelefonico,String rfc, Vehiculo []vehiculos, Propiedades []propiedades);
	
	public int actualizarArrendador(Bson filtro, String nombre, String apellidoPat, String apellidoMat, String curp,String edad, String numeroTelefonico,String rfc);
	
	public int borrarArrendador(Bson filtro);
	
	public ArrayList<String[]> mostrarPropiedadesDisponibles();
	
	public Propiedades buscarPropiedad(String rfc,String numeroReferencia);

	public Arrendador buscarArrendadorPorPropiedad(String identificador);

	public int actualizarPropiedadStatus(Boolean status, String identificador, String rfc);
	
}
