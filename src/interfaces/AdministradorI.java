package interfaces;

import java.util.ArrayList;

import org.bson.conversions.Bson;

import models.Administrador;
import models.Propiedades;
import models.Vehiculo;

public interface AdministradorI {
	
	public ArrayList<Administrador> verAdministrador();
	
	public int borrarAdministrador(Bson filtro);
	
	public int actualizarAdministrador(Bson filtro, String alias, String correo, String password);
	
	public void crearAdministrador( String alias, String correo, String password);

}
