package interfaces;

import org.bson.conversions.Bson;

import models.Entrada;
import models.Propiedades;

public interface PropiedadI {
	public void crearPropiedad(Bson filtro, Propiedades propiedad);
	public int actualizarPropiedad(Bson filtro, Bson filtroPropiedad, Propiedades propiedad);
	public int crearEntrada(Bson filtroGeneral, String filtroPropiedad, Entrada entrada);
	public int borrarPropiedad(Bson filtroRFC, String filtroNumeroReferencia);
}
