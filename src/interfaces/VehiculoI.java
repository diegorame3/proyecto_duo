package interfaces;

import org.bson.conversions.Bson;

import models.Vehiculo;

public interface VehiculoI {
	public void crearVehiculo(Vehiculo vehiculo, Bson filtro,String tipo);
	public int borrarVehiculo(Bson filtro, String filtroMatricula,String tipo);
	public int actualizarVehiculo(Vehiculo vehiculo, Bson filtroPersona, Bson filtroMatricula,String tipo);
}
