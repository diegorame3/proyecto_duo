package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.Document;
import org.bson.conversions.Bson;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.and;
import com.mongodb.client.MongoCollection;

import DAO.AdministradorDAO;
import config.ConexionMongo;

/**
 * Servlet implementation class Administrador
 */
@WebServlet("/Administrador")
public class Administrador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Administrador() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ConexionMongo con = new ConexionMongo();
		MongoCollection<Document> gradesCollection;
		RequestDispatcher rd;
		AdministradorDAO dao = new AdministradorDAO();
		
		
		if (request.getParameter("inicioDeSesion") != null) {
			try {
					String usuario = request.getParameter("usuario");
					String contrasena = request.getParameter("contrasena");
					
					gradesCollection = con.crearConexion().getCollection("Administrador");
					
					Document admin = gradesCollection.find(and(eq("correo",usuario),eq("password",contrasena))).first();
					
					if(admin != null) {
						rd = request.getRequestDispatcher("/vistas/adminPage/admin.jsp");
			            rd.forward(request, response);
					}else {
						System.out.println("No se encontro el usuario ingresado");
						rd = request.getRequestDispatcher("/vistas/login/login.jsp");
			            rd.forward(request, response);
					}
				

			} catch (Exception e) {
				System.out.println("Error en la conexion: "+e.getMessage());
				rd = request.getRequestDispatcher("/vistas/login/login.jsp");
	            rd.forward(request, response);
			}
		}
		
		if (request.getParameter("crearAdministrador") != null) {
			System.out.println("Entra crear administrador");
			dao.crearAdministrador(request.getParameter("nombre"), request.getParameter("correo"), request.getParameter("password"));
			rd = request.getRequestDispatcher("/vistas/administradores/administradores.jsp");
            rd.forward(request, response);
		}
		
		
		if (request.getParameter("borrarAdministrador") != null) {
			System.out.println("Entra crear administrador borrar    correo: "+request.getParameter("correo"));
			Bson filtro = eq("correo",request.getParameter("correo"));
			if (dao.borrarAdministrador(filtro) >= 1) {
				System.out.println("Se borr�");
				rd = request.getRequestDispatcher("/vistas/administradores/administradores.jsp");
				rd.forward(request, response);
			} else {
				System.out.println("Ocurrio un error mientras se eliminaba");
			}
		}
		
		if (request.getParameter("actualizarAdministrador") != null) {
			
			Bson filtro2 = eq("correo",request.getParameter("correo"));
			
			if (dao.actualizarAdministrador(filtro2,  request.getParameter("alias"), request.getParameter("correo"),request.getParameter("password")) >= 1) {
				System.out.println("Se actualiz� el administrador correctamente");
				rd = request.getRequestDispatcher("/vistas/administradores/administradores.jsp");
				rd.forward(request, response);
			} else {
				System.out.println("No se actualizo nada del administrador");
				rd = request.getRequestDispatcher("/vistas/administradores/administradores.jsp");
				rd.forward(request, response);
			}
			
			
		}
		
		
		
		
	}

}
