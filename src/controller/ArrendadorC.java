package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.ArrendadorDAO;
import DAO.PropiedadDAO;
import DAO.VehiculoDAO;
import models.Direccion;
import models.Entrada;
import models.Propiedades;
import models.Vehiculo;

import org.bson.conversions.Bson;
import static com.mongodb.client.model.Filters.eq;

/**
 * Servlet implementation class ArrendadorC
 */
@WebServlet("/ArrendadorC")
public class ArrendadorC extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ArrendadorC() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("hola doget");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd;
		try {

			// String accion = request.getParameter("botonAdministrador");
			String identificadorRFC = request.getParameter("rfc");
			Bson filtro = eq("rfc", identificadorRFC);

			if (request.getParameter("actualizar") != null) {
				String nombreNuevo = request.getParameter("nombre");
				String apellidoMatNuevo = request.getParameter("apellidoMat");
				String numeroTelefonicoNuevo = request.getParameter("numeroTelefonico");

				String apellidoPatNuevo = request.getParameter("apellidoPat");
				String curpNuevo = request.getParameter("curp");

				String edadNuevo = request.getParameter("edad");
				String rfcNuevo = request.getParameter("rfc");

				ArrendadorDAO arrendadorDAO = new ArrendadorDAO();

				if (arrendadorDAO.actualizarArrendador(filtro, nombreNuevo, apellidoPatNuevo, apellidoMatNuevo,
						curpNuevo, edadNuevo, numeroTelefonicoNuevo, rfcNuevo) >= 1) {
					System.out.println("Se actualiz� el documento correctamente");
					rd = request.getRequestDispatcher("/vistas/arrendadores/arrendadores.jsp");
					rd.forward(request, response);
				} else {
					System.out.println("No se actualizo nada del arrendador");
				}
			}

			if (request.getParameter("borrarArrendador") != null) {
				ArrendadorDAO arrendadorDAO = new ArrendadorDAO();

				if (arrendadorDAO.borrarArrendador(filtro) >= 1) {
					System.out.println("Se borr�");
					rd = request.getRequestDispatcher("/vistas/arrendadores/arrendadores.jsp");
					rd.forward(request, response);
				} else {
					System.out.println("Ocurrio un error mientras se eliminaba");
				}

			}

			if (request.getParameter("crearArrendatario") != null) {
				String nombre = request.getParameter("nombre");
				String apellidoPat = request.getParameter("apellidoPat");
				String apellidoMat = request.getParameter("apellidoMat");

				String curp = request.getParameter("curp");
				String edad = request.getParameter("edad");
				String numeroTelefonico = request.getParameter("numeroTelefonico");

				String rfc = request.getParameter("rfc");
				Propiedades[] propiedades = null;
				Vehiculo[] vehiculos = null;
				ArrendadorDAO arrendadorDAO = new ArrendadorDAO();

				arrendadorDAO.crearArrendador(nombre, apellidoPat, apellidoMat, curp, edad, numeroTelefonico, rfc,
						vehiculos, propiedades);

				rd = request.getRequestDispatcher("/vistas/arrendadores/arrendadores.jsp");
				rd.forward(request, response);
			}

			if (request.getParameter("verAutomovil") != null) {
				rd = request.getRequestDispatcher(
						"/vistas/autos/autos.jsp?tipo=arrendador&rfc=" + request.getParameter("rfc"));
				rd.forward(request, response);
			}

			if (request.getParameter("crearAutomovil") != null) {
				Vehiculo nuevoVehiculo = new Vehiculo();

				nuevoVehiculo.setMatricula(request.getParameter("matricula"));
				nuevoVehiculo.setTipo(request.getParameter("tipo"));
				nuevoVehiculo.setMarca(request.getParameter("marca"));

				VehiculoDAO vehiculoDao = new VehiculoDAO();

				vehiculoDao.crearVehiculo(nuevoVehiculo, filtro, "arrendador");

				rd = request.getRequestDispatcher("/vistas/arrendadores/arrendadores.jsp");
				rd.forward(request, response);

			}

			if (request.getParameter("actualizarAutomovil") != null) {
				Vehiculo nuevoVehiculo = new Vehiculo();
				System.out.println("entra a actualiza vehiculo en arrendador");
				nuevoVehiculo.setMatricula(request.getParameter("matricula"));
				nuevoVehiculo.setTipo(request.getParameter("tipo"));
				nuevoVehiculo.setMarca(request.getParameter("marca"));

				Bson filtroMatricula = eq("vehiculos.matricula", request.getParameter("matricula"));

				VehiculoDAO vehiculoDao = new VehiculoDAO();

				int respuesta = vehiculoDao.actualizarVehiculo(nuevoVehiculo, filtro, filtroMatricula, "arrendador");

				rd = request.getRequestDispatcher("/vistas/arrendadores/arrendadores.jsp");
				rd.forward(request, response);
			}

			if (request.getParameter("borrarAutomovil") != null) {
				String matricula = request.getParameter("matricula");

				VehiculoDAO vehiculoDao = new VehiculoDAO();

				vehiculoDao.borrarVehiculo(filtro, matricula, "arrendador");

				rd = request.getRequestDispatcher("/vistas/arrendadores/arrendadores.jsp");
				rd.forward(request, response);
			}

			if (request.getParameter("verPropiedad") != null) {
				rd = request.getRequestDispatcher(
						"/vistas/propiedades/propiedades.jsp?tipo=arrendador&rfc=" + request.getParameter("rfc"));
				rd.forward(request, response);
			}

			if (request.getParameter("crearPropiedad") != null) {

				PropiedadDAO actualizarPropiedad = new PropiedadDAO();

				Propiedades propiedad = new Propiedades();
				Direccion direccion = new Direccion();

				propiedad.setEstatusRenta(false);
				propiedad.setDescripcion(request.getParameter("descripcion"));
				propiedad.setNumeroReferencia(request.getParameter("numeroReferencia"));

				direccion.setCalle(request.getParameter("calle"));
				direccion.setCp(request.getParameter("cp"));
				direccion.setNumero(request.getParameter("numero"));
				direccion.setZona(request.getParameter("zona"));

				propiedad.setDireccion(direccion);

				actualizarPropiedad.crearPropiedad(filtro, propiedad);

				rd = request.getRequestDispatcher("/vistas/arrendadores/arrendadores.jsp");
				rd.forward(request, response);
			}

			if (request.getParameter("actualizarPropiedad") != null) {
				
				Bson filtroPropiedad = eq("propiedades.numeroReferencia", request.getParameter("numeroReferencia"));

				PropiedadDAO actualizarPropiedad = new PropiedadDAO();
				Propiedades nuevaPropiedad = new Propiedades();

				nuevaPropiedad.setEstatusRenta(Boolean.parseBoolean(request.getParameter("estatus")));
				nuevaPropiedad.setDescripcion(request.getParameter("descripcion"));
				nuevaPropiedad.setNumeroReferencia(request.getParameter("numeroReferencia"));

				Direccion direccion = new Direccion();

				direccion.setCalle(request.getParameter("calle"));
				direccion.setCp(request.getParameter("cp"));
				direccion.setNumero(request.getParameter("numero"));
				direccion.setZona(request.getParameter("zona"));

				nuevaPropiedad.setDireccion(direccion);

				actualizarPropiedad.actualizarPropiedad(filtro, filtroPropiedad, nuevaPropiedad);

				rd = request.getRequestDispatcher("/vistas/arrendadores/arrendadores.jsp");
				rd.forward(request, response);
			}
			
			if (request.getParameter("borrarPropiedad") != null) {
				PropiedadDAO propiedadDAO = new PropiedadDAO();
				
				String numeroReferencia = request.getParameter("numeroReferencia");
				
				propiedadDAO.borrarPropiedad(filtro, numeroReferencia);
				
				rd = request.getRequestDispatcher(
						"/vistas/arrendadores/arrendadores.jsp");
				rd.forward(request, response);
			}

			if (request.getParameter("crearEntrada") != null) {
				
				PropiedadDAO propiedadDao = new PropiedadDAO();
				
				Entrada nuevaEntrada = new Entrada();
				nuevaEntrada.setNombre(request.getParameter("entrada"));
				
				propiedadDao.crearEntrada(filtro, request.getParameter("numeroReferencia"), nuevaEntrada);
				
				rd = request.getRequestDispatcher("/vistas/arrendadores/arrendadores.jsp");
				rd.forward(request, response);
			}
		} catch (Exception e) {
			System.out.println("error: " + e.getMessage());
		}

	}

}
