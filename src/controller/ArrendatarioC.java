package controller;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.google.gson.Gson;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

import DAO.ArrendadorDAO;
import DAO.ArrendatarioDAO;
import DAO.VehiculoDAO;
import config.ConexionMongo;
import models.Arrendador;
import models.Arrendatario;
import models.Aval;
import models.Propiedades;
import models.Vehiculo;

/**
 * Servlet implementation class ArrendatarioC
 */
@WebServlet("/ArrendatarioC")
public class ArrendatarioC extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ArrendatarioC() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());

		ArrendatarioDAO dao = new ArrendatarioDAO();
		RequestDispatcher rd;

		
		if (request.getParameter("crearArrendatario") != null) {
			String nombre = request.getParameter("nombre");
			String apellidoPat = request.getParameter("apellidoPat");
			String apellidoMat = request.getParameter("apellidoMat");

			String curp = request.getParameter("curp");
			String edad = request.getParameter("edad");
			String numeroTelefonico = request.getParameter("numeroTelefono");

			String rfc = request.getParameter("rfc");
			String fechaContrato = request.getParameter("fechaContrato");
			String pago = request.getParameter("pago");
			String numeroReferenciaPropiedad="-1";
			
			Vehiculo[] vehiculos = null;
			Aval[] aval = null;
			ArrendatarioDAO arrendatarioDAO = new ArrendatarioDAO();

			arrendatarioDAO.crearArrendatario(nombre, apellidoPat, apellidoMat, curp, edad, numeroTelefonico, rfc, vehiculos, numeroReferenciaPropiedad,aval,pago,fechaContrato);

			rd = request.getRequestDispatcher("/vistas/arrendatarios/arrendatarios.jsp");
			rd.forward(request, response);
		}
		
		
		if (request.getParameter("borrar") != null) {
			if (dao.borrarArrendatario(request.getParameter("rfc")) != null) {
				rd = request.getRequestDispatcher("/vistas/arrendatarios/arrendatarios.jsp");
				rd.forward(request, response);
			} else {
				System.out.println("Ocurrio un error mientras se eliminaba");
			}
		}

		if (request.getParameter("verAval") != null) {
			rd = request.getRequestDispatcher(
					"vistas/avales/aval.jsp?tipo=arrendatario&rfc=" + request.getParameter("rfc"));
			rd.forward(request, response);
		}

		if (request.getParameter("verPropiedad") != null) {
			rd = request.getRequestDispatcher(
					"vistas/propiedades/propiedadArrendatario.jsp?tipo=arrendatario&rfc=" + request.getParameter("rfc"));
			rd.forward(request, response);
		}

		if (request.getParameter("verAutomovil") != null) {
			rd = request.getRequestDispatcher(
					"/vistas/autos/autos.jsp?tipo=arrendatario&rfc=" + request.getParameter("rfc"));
			rd.forward(request, response);
		}

		if (request.getParameter("actualizar") != null) {
			if (dao.actualizarArrendatario(request.getParameter("rfc"), request.getParameter("nombre"),
					request.getParameter("apellidoMat"), request.getParameter("apellidoPat"),
					request.getParameter("numeroTelefono"), request.getParameter("curp"), request.getParameter("edad"))
					.getModifiedCount() >= 1) {
				System.out.println("Se actualizó el documento correctamente");
				rd = request.getRequestDispatcher("/vistas/arrendatarios/arrendatarios.jsp");
				rd.forward(request, response);

			} else {
				System.out.println("No se actualizo nada");
				rd = request.getRequestDispatcher("/vistas/arrendatarios/arrendatarios.jsp");
				rd.forward(request, response);
			}

		}

		if (request.getParameter("crearAutomovil") != null) {

			Vehiculo nuevoVehiculo = new Vehiculo();
			String identificadorRFC = request.getParameter("rfc");
			Bson filtro = eq("rfc", identificadorRFC);

			nuevoVehiculo.setMatricula(request.getParameter("matricula"));
			nuevoVehiculo.setTipo(request.getParameter("tipo"));
			nuevoVehiculo.setMarca(request.getParameter("marca"));

			VehiculoDAO vehiculoDao = new VehiculoDAO();

			vehiculoDao.crearVehiculo(nuevoVehiculo, filtro, "arrendatario");

			rd = request.getRequestDispatcher("/vistas/arrendatarios/arrendatarios.jsp");
			rd.forward(request, response);

		}

		if (request.getParameter("actualizarAutomovil") != null) {
			Vehiculo nuevoVehiculo = new Vehiculo();
			String identificadorRFC = request.getParameter("rfc");
			Bson filtro = eq("rfc", identificadorRFC);
			System.out.println("entra a actualiza vehiculo en arrendador rfc: " + request.getParameter("rfc")
					+ "matricula: " + request.getParameter("matricula"));
			nuevoVehiculo.setMatricula(request.getParameter("matricula"));
			nuevoVehiculo.setTipo(request.getParameter("tipo"));
			nuevoVehiculo.setMarca(request.getParameter("marca"));

			Bson filtroMatricula = eq("vehiculos.matricula", request.getParameter("matricula"));

			VehiculoDAO vehiculoDao = new VehiculoDAO();

			int respuesta = vehiculoDao.actualizarVehiculo(nuevoVehiculo, filtro, filtroMatricula, "arrendatario");

			rd = request.getRequestDispatcher("/vistas/arrendatarios/arrendatarios.jsp");
			rd.forward(request, response);
		}

		if (request.getParameter("borrarAutomovil") != null) {
			System.out.println("entra a borrar auto en arrendatario matricula:" + request.getParameter("matricula"));
			String matricula = request.getParameter("matricula");
			String identificadorRFC = request.getParameter("rfc");
			Bson filtro = eq("rfc", identificadorRFC);
			VehiculoDAO vehiculoDao = new VehiculoDAO();

			vehiculoDao.borrarVehiculo(filtro, matricula, "arrendatario");

			rd = request.getRequestDispatcher("/vistas/arrendatarios/arrendatarios.jsp");
			rd.forward(request, response);
		}
		
		if (request.getParameter("añadirPropiedadRenta") != null) {

			if (dao.actualizarArrendatario(request.getParameter("rfc"),request.getParameter("categorias").split(":")[2]) 
					.getModifiedCount() >= 1) {
				System.out.println("Se actualizó el documento correctamente");
				//actualizamos el estatus de la propiedad a rentado
				ArrendadorDAO daoArrendador = new ArrendadorDAO();
				daoArrendador.actualizarPropiedadStatus(true,request.getParameter("categorias").split(":")[2],daoArrendador.buscarArrendadorPorPropiedad(request.getParameter("categorias").split(":")[2]).getRFC());
				
				
				rd = request.getRequestDispatcher("/vistas/arrendatarios/arrendatarios.jsp");
				rd.forward(request, response);

			} else {
				System.out.println("No se actualizo nada en propiedad rentada");
			}
		}
		
		if (request.getParameter("eliminaPropiedadRenta") != null) {
			
			if (dao.actualizarArrendatario(request.getParameter("rfc"),"-1") 
					.getModifiedCount() >= 1) {
				System.out.println("Se actualizó el documento correctamente");
				rd = request.getRequestDispatcher("/vistas/arrendatarios/arrendatarios.jsp");
				//actualizamos el estatus de la propiedad a no rentado
				ArrendadorDAO daoArrendador = new ArrendadorDAO();
				daoArrendador.actualizarPropiedadStatus(false,request.getParameter("identificador"),daoArrendador.buscarArrendadorPorPropiedad(request.getParameter("identificador")).getRFC());
				
				rd.forward(request, response);

			} else {
				System.out.println("No se actualizo nada");
			}
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
