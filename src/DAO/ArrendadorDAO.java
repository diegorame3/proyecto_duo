package DAO;

import java.util.ArrayList;
import java.util.Iterator;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Projections;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

import config.ConexionMongo;
import interfaces.ArrendadorI;
import models.Arrendador;
import models.Arrendatario;
import models.Propiedades;
import models.Vehiculo;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;
import static java.util.Arrays.asList;

import org.bson.types.ObjectId;

public class ArrendadorDAO implements ArrendadorI {

	@Override
	public ArrayList<Arrendador> verArrendador() {
		ConexionMongo con = new ConexionMongo();
		MongoCollection<Document> gradesCollection;
		Gson gson = new Gson();
		ArrayList<Arrendador> listaArrendador = new ArrayList<>();
		Arrendador arrendador;

		try {
			gradesCollection = con.crearConexion().getCollection("Arrendador");

			FindIterable<Document> admin = gradesCollection.find();

			for (Document document : admin) {
				arrendador = new Arrendador();
				//System.out.println(document.toJson());
				arrendador = gson.fromJson(document.toJson(), Arrendador.class);
				listaArrendador.add(arrendador);
			}
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
		return listaArrendador;
	}

	@Override
	public Arrendador buscarArrendador(String rfc) {
			ConexionMongo con = new ConexionMongo();
			MongoCollection<Document> gradesCollection;
			Gson gson = new Gson();
			Arrendador arrendador = null ;
			
			try {
				
				gradesCollection = con.crearConexion().getCollection("Arrendador");
				BasicDBObject filtro = new BasicDBObject();
				filtro.put("rfc", rfc);
				FindIterable<Document>  admin = gradesCollection.find(filtro);
				
				System.out.println("Entra al iteravle ");
				for (Document document : admin) {
					arrendador= new Arrendador();
					//deserealizar
					System.out.println(document.toJson());
					arrendador = gson.fromJson(document.toJson(), Arrendador.class);
					
				}
			} catch (Exception e) {
				System.out.println("Error al leer arrendatarios: "+e);
			}
			return arrendador;
		}
	
	@Override
	public Arrendador buscarArrendadorPorPropiedad(String identificador) {
			ConexionMongo con = new ConexionMongo();
			MongoCollection<Document> gradesCollection;
			Gson gson = new Gson();
			Arrendador arrendador = null ;
			
			try {
				
				gradesCollection = con.crearConexion().getCollection("Arrendador");
				BasicDBObject filtro = new BasicDBObject();
				Bson filtro2 = eq("propiedades.numeroReferencia", identificador);
				
				FindIterable<Document>  admin = gradesCollection.find(filtro2);
				
				System.out.println("Entra al iteravle ");
				for (Document document : admin) {
					arrendador= new Arrendador();
					//deserealizar
					System.out.println(document.toJson());
					arrendador = gson.fromJson(document.toJson(), Arrendador.class);
					
				}
			} catch (Exception e) {
				System.out.println("Error al leer arrendatarios: "+e);
			}
			return arrendador;
		}

	@Override
	public void crearArrendador(String nombre, String apellidoPat, String apellidoMat, String curp, String edad,
			String numeroTelefonico, String rfc, Vehiculo []vehiculos, Propiedades []propiedades) {
		
		ConexionMongo con = new ConexionMongo();
		MongoCollection<Document> gradesCollection;
		
		try {
			gradesCollection = con.crearConexion().getCollection("Arrendador");
			
			Document arrendador = new Document("_id", new ObjectId());
			arrendador.append("nombre", nombre)
				.append("apellidoPat", apellidoPat)
				.append("apellidoMat", apellidoMat)
				.append("curp", curp)
				.append("edad", edad)
				.append("numeroTelefonico", numeroTelefonico)
				.append("rfc", rfc)
				.append("propiedades", asList())
				.append("vehiculos", asList());
			
			gradesCollection.insertOne(arrendador);
		} catch (Exception e) {
			System.out.println("Error en crear arrendatario: "+e.getMessage());
		}
	}

	@Override
	public int actualizarArrendador(Bson filtro, String nombre, String apellidoPat, String apellidoMat, String curp, String edad,
			String numeroTelefonico, String rfc) {
		ConexionMongo con = new ConexionMongo();
		int resultado=0;
		try {
			MongoCollection<Document> gradesCollection = con.crearConexion().getCollection("Arrendador");
			
			Bson update1 = set("nombre", nombre);
			Bson update2 = set("apellidoMat", apellidoMat);
			Bson update3 = set("numeroTelefonico", numeroTelefonico);
			Bson update5 = set("apellidoPat", apellidoPat);
			Bson update6 = set("curp", curp);
			Bson update8 = set("edad", edad);
			Bson update9 = set("rfc", rfc);
			
			Bson updates = combine(update1, update2, update3, update5, update6, update8, update9);
			
			UpdateResult updateResult = gradesCollection.updateOne(filtro, updates);
			resultado = (int) updateResult.getModifiedCount();
		} catch (Exception e) {
			System.out.println("Error al actualizar arrendador: "+e.getMessage());
			resultado = 0;
		}
		
		return resultado;
	}
	
	@Override
	public int actualizarPropiedadStatus(Boolean status, String identificador, String rfc) {
		System.out.println("entra a actualizarStatus:"+status.toString()+" "+identificador+"  "+rfc);
		ConexionMongo con = new ConexionMongo();
		int resultado = 0;
		MongoCollection<Document> gradesCollection;
		Bson filtroIdentificador = eq("propiedades.numeroReferencia", identificador);
		Bson filtro = eq("rfc", rfc);
		try {
				gradesCollection = con.crearConexion().getCollection("Arrendador");
			
			Bson filter = and(filtro,filtroIdentificador);
			
			Bson updateTipo = set("propiedades.$.estatusRenta", status);
			
			
			Bson updates = combine(updateTipo);
			
			UpdateResult updateResult = gradesCollection.updateOne(filter, updateTipo);
			resultado = (int) updateResult.getModifiedCount();
		} catch (Exception e) {
			System.out.println("Error al actualizar status: "+e.getMessage());
			resultado = 0;
		}
		
		return resultado;
	}

	@Override
	public int borrarArrendador(Bson filtro) {
		ConexionMongo con = new ConexionMongo();
		int resultado = 0;
		try {
			MongoCollection<Document> gradesCollection = con.crearConexion().getCollection("Arrendador");
			
			DeleteResult result = gradesCollection.deleteOne(filtro);
			
			resultado= (int) result.getDeletedCount();
		} catch (Exception e) {
			System.out.println("Error mientras se borraba arrendador en dao");
			resultado=0;
		}
			
		return resultado;
	}
	

	public ArrayList<String[]> mostrarPropiedadesDisponibles() {
		ArrayList<Arrendador> listaArrendadores = verArrendador();
		ArrayList<String[]> listaDisponibles = new ArrayList<String[]>();
		String[] a;
		//recorre arrendadores
		for (Arrendador arrendador : listaArrendadores) {
			//recorremos las propiedades
			for (Propiedades propiedad : arrendador.getPropiedades()) {
				
				if (!propiedad.getEstatusRenta()) {
					 //String[] fruit = {propiedad.getNumeroReferencia(), "Banana"};
					a= new String[2];
					a[0]=propiedad.getNumeroReferencia();
					a[1]=arrendador.getNombre();
					listaDisponibles.add(a);
				}
				System.out.println(propiedad.getEstatusRenta()+"   "+propiedad.getNumeroReferencia()+"  "+arrendador.getNombre());
			}
		}
		return  listaDisponibles;
		
	}
	
	
	@Override
	public Propiedades buscarPropiedad(String rfc,String numeroReferencia) {
		ConexionMongo con = new ConexionMongo();
		MongoCollection<Document> gradesCollection;
		Gson gson = new Gson();
		Arrendador arrendador = null ;
		Propiedades propiedad = new Propiedades();
		
		Bson filtroIdentificador = eq("propiedades.numeroReferencia", numeroReferencia);
		Bson filtro = eq("rfc", rfc);
		Bson filter = and(filtro,filtroIdentificador);
		System.out.println("rfc:"+rfc+"   numref:  "+numeroReferencia);
		try {
			
			gradesCollection = con.crearConexion().getCollection("Arrendador");


			FindIterable<Document>  admin = gradesCollection.find(filter);
			
			for (Document document : admin) {
				arrendador= new Arrendador();
				//deserealizar
				System.out.println("documento encontrado en buscarPropiedad  "+document.toJson());
				arrendador = gson.fromJson(document.toJson(), Arrendador.class);
				//iteramos para buscar el numero de referencia correspondiente
				for (Propiedades prop : arrendador.getPropiedades()) {
					if (prop.getNumeroReferencia().equals(numeroReferencia)) {
						propiedad=prop;
						System.out.println("propiedad retornada: "+propiedad.getNumeroReferencia());
					}
				}
				
			}
		} catch (Exception e) {
			System.out.println("Error al leer arrendatarios: "+e);
		}
		return propiedad;
	}
}
