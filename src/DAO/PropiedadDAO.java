package DAO;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;
import static com.mongodb.client.model.Updates.push;
import static java.util.Arrays.asList;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.UpdateResult;

import config.ConexionMongo;
import interfaces.PropiedadI;
import models.Entrada;
import models.Propiedades;
import static com.mongodb.client.model.Filters.*;

public class PropiedadDAO implements PropiedadI{

	@Override
	public void crearPropiedad(Bson filtro, Propiedades propiedad) {
		ConexionMongo con = new ConexionMongo();
		MongoCollection<Document> gradesCollection;
		
		try {
			gradesCollection = con.crearConexion().getCollection("Arrendador");
			
			Document newPropiedad = new Document()
					.append("estatusRenta", false)
					.append("descripcion", propiedad.getDescripcion())
					.append("numeroReferencia", propiedad.getNumeroReferencia())
					.append("entradas", asList())
					.append("direccion", new Document("calle", propiedad.getDireccion().getCalle())
											.append("numero", propiedad.getDireccion().getNumero())
											.append("zona", propiedad.getDireccion().getZona())
											.append("cp", propiedad.getDireccion().getCp()));
			
			gradesCollection.updateOne(filtro, Updates.addToSet("propiedades", newPropiedad));
			
		} catch (Exception e) {
			System.out.println("Error al crear propiedad: "+e.getMessage());
		}
	}

	@Override
	public int actualizarPropiedad(Bson filtro, Bson filtroPropiedad, Propiedades propiedad) {
		ConexionMongo con = new ConexionMongo();
		int resultado = 0;
		MongoCollection<Document> gradesCollection;
		try {
			gradesCollection = con.crearConexion().getCollection("Arrendador");
					
			Bson filter = and(filtro,filtroPropiedad);
			
			Bson updateStatusRenta = set("propiedades.$.estatusRenta", propiedad.getEstatusRenta());
			Bson updateDescripcion = set("propiedades.$.descripcion", propiedad.getDescripcion());
			
			Bson updateDireccionCalle = set("propiedades.$.direccion.calle", propiedad.getDireccion().getCalle());
			Bson updateDireccionNumero = set("propiedades.$.direccion.numero", propiedad.getDireccion().getNumero());
			Bson updateDireccionZona = set("propiedades.$.direccion.zona", propiedad.getDireccion().getZona());
			
			Bson updates = combine(updateStatusRenta,updateDescripcion,updateDireccionCalle,updateDireccionNumero,updateDireccionZona);
			
			UpdateResult updateResult = gradesCollection.updateOne(filter, updates);
			
			resultado = (int) updateResult.getModifiedCount();
			System.out.println("Resultado para actualizar propiedad: "+resultado);
			
		} catch (Exception e) {
			System.out.println("error al actualizar propiedad: "+e.getMessage());
			resultado=0;
		}
		return resultado;
	}

	@Override
	public int crearEntrada(Bson filtroGeneral, String filtroPropiedad, Entrada entrada) {
		ConexionMongo con = new ConexionMongo();
		MongoCollection<Document> gradesCollection;
		int resultado = 0;
		try {
			gradesCollection = con.crearConexion().getCollection("Arrendador");
			
			UpdateOptions options = new UpdateOptions()
			                              .arrayFilters(asList(eq("prop.numeroReferencia", filtroPropiedad)));
			
			Bson update = push("propiedades.$[prop].entradas", new Document() 
															.append("nombre", entrada.getNombre()));
			
			UpdateResult result = gradesCollection.updateOne(filtroGeneral, update, options);
			
			resultado = (int) result.getModifiedCount();
			System.out.println("Resultado para crear entrada: "+resultado);
		} catch (Exception e) {
			System.out.println("Error mientras se creaba una entrada: "+e.getMessage());
			resultado = 0;
		}
		return resultado;
	}

	@Override
	public int borrarPropiedad(Bson filtroRFC, String filtroNumeroReferencia) {
		ConexionMongo con = new ConexionMongo();
		int resultado = 1;
		MongoCollection<Document> gradesCollection;
		
		try {
			gradesCollection = con.crearConexion().getCollection("Arrendador");
			
			gradesCollection.updateOne(filtroRFC, Updates.pull("propiedades", new Document("numeroReferencia",filtroNumeroReferencia)));

			
			
		} catch (Exception e) {
			System.out.println("Error borrando propiedad de arrendador: "+e.getMessage());
			resultado=0;
		}
		
		return resultado;
	}

	
}
