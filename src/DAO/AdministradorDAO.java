package DAO;

import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;
import static java.util.Arrays.asList;

import java.util.ArrayList;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.google.gson.Gson;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

import config.ConexionMongo;
import interfaces.AdministradorI;
import models.Administrador;

public class AdministradorDAO implements AdministradorI{

	@Override
	public ArrayList<Administrador> verAdministrador() {
		ConexionMongo con = new ConexionMongo();
		MongoCollection<Document> gradesCollection;
		Gson gson = new Gson();
		ArrayList<Administrador> lista = new ArrayList();
		Administrador administrador;
		
		try {
			
			gradesCollection = con.crearConexion().getCollection("Administrador");
			System.out.println(gradesCollection);
			FindIterable<Document>  admin = gradesCollection.find();
			
			for (Document document : admin) {
				administrador= new Administrador();
				//deserealizar
				System.out.println(document.toJson());
				administrador = gson.fromJson(document.toJson(), Administrador.class);
				lista.add(administrador);
				//System.out.println("Nombre "+arrendatario.getNombre());
				//System.out.println("Edad: "+arrendatario.getEdad());
				//System.out.println("NOmbre Aval: "+arrendatario.getAval()[0].getNombre());
			}
		} catch (Exception e) {
			System.out.println("Error al leer arrendatarios: "+e);
		}
		return lista;
	}

	@Override
	public int borrarAdministrador(Bson filtro) {
		ConexionMongo con = new ConexionMongo();
		int resultado = 0;
		try {
			MongoCollection<Document> gradesCollection = con.crearConexion().getCollection("Administrador");
			
			DeleteResult result = gradesCollection.deleteOne(filtro);
			
			resultado= (int) result.getDeletedCount();
		} catch (Exception e) {
			System.out.println("Error mientras se borraba administrador en el dao");
			resultado=0;
		}
			
		return resultado;
	}

	@Override
	public int actualizarAdministrador(Bson filtro, String alias, String correo, String password) {
		System.out.println("Entra a actualizar admin");
		System.out.println("Entra crear administrador    correo: "+correo+"  alias:"+alias+"   password:"+password);
		ConexionMongo con = new ConexionMongo();
		int resultado=0;
		try {
			MongoCollection<Document> gradesCollection = con.crearConexion().getCollection("Administrador");
			
			Bson update1 = set("alias", alias);
			Bson update2 = set("correo", correo);
			Bson update3 = set("password", password);
			
			Bson updates = combine(update1,update2, update3);
			
			UpdateResult updateResult = gradesCollection.updateOne(filtro, updates);
			resultado = (int) updateResult.getModifiedCount();
			System.out.println("res= "+resultado);
		} catch (Exception e) {
			System.out.println("Error al actualizar arrendador"+e.getMessage());
			resultado = 0;
		}
		
		return resultado;

	}

	@Override
	public void crearAdministrador(String alias, String correo, String password) {
		ConexionMongo con = new ConexionMongo();
		MongoCollection<Document> gradesCollection;
		
		try {
			gradesCollection = con.crearConexion().getCollection("Administrador");
			
			Document arrendatario = new Document("_id", new ObjectId());
			arrendatario.append("alias", alias)
				.append("correo", correo)
				.append("password", password);
			
			gradesCollection.insertOne(arrendatario);
		} catch (Exception e) {
			System.out.println("Error en crear Administrador: "+e.getMessage());
		}
		
	}
}


