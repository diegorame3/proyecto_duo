package DAO;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;
import static java.util.Arrays.asList;

import java.util.ArrayList;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Projections;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

import config.ConexionMongo;
import interfaces.ArrendadorI;
import interfaces.ArrendatarioI;
import models.Arrendatario;
import models.Aval;
import models.Propiedades;
import models.Vehiculo;

public class ArrendatarioDAO implements ArrendatarioI{
	
	MongoCollection<Document> gradesCollection;
	ConexionMongo con = new ConexionMongo();

	@Override
	public ArrayList<Arrendatario> verArrendatarios() {
		ConexionMongo con = new ConexionMongo();
		MongoCollection<Document> gradesCollection;
		Gson gson = new Gson();
		Arrendatario arrendatario;
		ArrayList<Arrendatario> lista = new ArrayList<>();
		
		try {
			
			gradesCollection = con.crearConexion().getCollection("Arrendatario");
			System.out.println(gradesCollection);
			FindIterable<Document>  admin = gradesCollection.find();
			
			for (Document document : admin) {
				arrendatario= new Arrendatario();
				//deserealizar
				System.out.println(document.toJson());
				arrendatario = gson.fromJson(document.toJson(), Arrendatario.class);
				lista.add(arrendatario);
			}
		} catch (Exception e) {
			System.out.println("Error al leer arrendatarios: "+e);
		}
		return lista;
	}

	@Override
	public Arrendatario buscarArrendatario(String rfc) {
		ConexionMongo con = new ConexionMongo();
		MongoCollection<Document> gradesCollection;
		Gson gson = new Gson();
		Arrendatario arrendatario = null ;
		
		try {
			
			gradesCollection = con.crearConexion().getCollection("Arrendatario");
			BasicDBObject filtro = new BasicDBObject();
			filtro.put("rfc", rfc);
			FindIterable<Document>  admin = gradesCollection.find(filtro);
			System.out.println("Entra al iteravle ");
			for (Document document : admin) {
				arrendatario= new Arrendatario();
				//deserealizar
				System.out.println(document.toJson());
				arrendatario = gson.fromJson(document.toJson(), Arrendatario.class);
				
			}
		} catch (Exception e) {
			System.out.println("Error al leer arrendatarios: "+e);
		}
		return arrendatario;
	}
	
	@Override
	public void crearArrendatario(String nombre, String apellidoPat, String apellidoMat, String curp, String edad,
			String numeroTelefonico, String rfc, Vehiculo []vehiculos, String numeroReferenciaPropiedad,Aval[] aval,String pago,String fechaContrato) {
		
		ConexionMongo con = new ConexionMongo();
		MongoCollection<Document> gradesCollection;
		
		try {
			gradesCollection = con.crearConexion().getCollection("Arrendatario");
			
			Document arrendatario = new Document("_id", new ObjectId());
			arrendatario.append("nombre", nombre)
				.append("apellidoPat", apellidoPat)
				.append("apellidoMat", apellidoMat)
				.append("curp", curp)
				.append("edad", edad)
				.append("numeroTelefono", numeroTelefonico)
				.append("rfc", rfc)
				.append("fechaContrato", fechaContrato)
				.append("formaPago", pago)
				.append("numeroReferenciaPropiedad",numeroReferenciaPropiedad)
				.append("vehiculos", asList())
			.append("aval", asList());
			
			gradesCollection.insertOne(arrendatario);
		} catch (Exception e) {
			System.out.println("Error en crear arrendatario: "+e.getMessage());
		}
	}
	

	@Override
	public DeleteResult borrarArrendatario(String rfc) {
		
		DeleteResult result=null;
		try {
			gradesCollection = con.crearConexion().getCollection("Arrendatario");
			
			Bson filtro = eq("rfc",rfc );
			result = gradesCollection.deleteOne(filtro);
		} catch (Exception e) {
			System.out.println("Error al crear la conexion...");
		}
		return result;
	}

	@Override
	public  UpdateResult actualizarArrendatario(String rfc, String nombre, String apellidoMat, String apellidoPat,
		String numeroTelefono, String curp, String edad) {
		 
		Bson filtro = eq("rfc",rfc);
		UpdateResult updateResult=null;
		
		Bson update1 = set("nombre", nombre);
		Bson update2 = set("apellidoMat", apellidoMat);
		Bson update3 = set("numeroTelefono", numeroTelefono);
		Bson update5 = set("apellidoPat", apellidoPat);
		Bson update6 = set("curp", curp);
		Bson update8 = set("edad", edad);
		Bson update9 = set("rfc", rfc);
		
		Bson updates = combine(update1, update2, update3, update5, update6, update8, update9);
		
		
		try {
			gradesCollection = con.crearConexion().getCollection("Arrendatario");
			updateResult = gradesCollection.updateOne(filtro, updates);
		} catch (Exception e) {
			System.out.println("Error al crear la conexion");
		}
		
        
        
		
		return updateResult;
	}
	
	@Override
	public  UpdateResult actualizarArrendatario(String rfc,String numeroReferenciaPropiedad) {
		 
		//falta cambiar el estado de la propiedad que esta rentando
		
		Bson filtro = eq("rfc",rfc);
		UpdateResult updateResult=null;
		
		Bson update1 = set("numeroReferenciaPropiedad", numeroReferenciaPropiedad);
		
		
		Bson updates = combine(update1);
		
		
		try {
			gradesCollection = con.crearConexion().getCollection("Arrendatario");
			updateResult = gradesCollection.updateOne(filtro, updates);
		} catch (Exception e) {
			System.out.println("Error al crear la conexion");
		}
		return updateResult;
	}

	@Override
	public Arrendatario buscarPropiedad(String numeroReferencia) {
		ConexionMongo con = new ConexionMongo();
		MongoCollection<Document> gradesCollection;
		Gson gson = new Gson();
		Arrendatario arrendatario = null ;
		
		try {
			
			gradesCollection = con.crearConexion().getCollection("Arrendatario");
			BasicDBObject filtro = new BasicDBObject();
			filtro.put("rfc", numeroReferencia);
			FindIterable<Document>  admin = gradesCollection.find(filtro).projection(Projections.include("propiedades"));
			System.out.println("Entra al iterale ");
			for (Document document : admin) {
				arrendatario= new Arrendatario();
				//deserealizar
				System.out.println(document.toJson());
				arrendatario = gson.fromJson(document.toJson(), Arrendatario.class);
				System.out.println(arrendatario.getNumeroReferenciaPropiedad());
			}
		} catch (Exception e) {
			System.out.println("Error al leer arrendatarios: "+e);
		}
		return arrendatario;
	}
}
