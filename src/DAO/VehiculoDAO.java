package DAO;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.UpdateResult;

import config.ConexionMongo;
import interfaces.VehiculoI;
import models.Vehiculo;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

public class VehiculoDAO implements VehiculoI{

	@Override
	public void crearVehiculo(Vehiculo vehiculo, Bson filtro,String tipo) {
		ConexionMongo con = new ConexionMongo();
		MongoCollection<Document> gradesCollection;
		
		try {
			if (tipo.equals("arrendador")) {
				gradesCollection = con.crearConexion().getCollection("Arrendador");
			}else {
				gradesCollection = con.crearConexion().getCollection("Arrendatario");
			}
		 
			
			Document newVehiculo = new Document()
					.append("matricula", vehiculo.getMatricula())
					.append("tipo", vehiculo.getTipo())
					.append("marca", vehiculo.getMarca());
			
			gradesCollection.updateOne(filtro, Updates.addToSet("vehiculos", newVehiculo));
			
			/*Document arrendador = new Document("rfc", "VAVCQRX");
			arrendador.append("vehiculos", asList(
					new Document("matricula", vehiculo.getMatricula())
					.append("tipo", vehiculo.getTipo())
					.append("marca", vehiculo.getMarca())
					));*/
			//Bson update = set
			//Bson update = set("vehiculos", );
			
			//Bson update2 = set({},{$set});
			//UpdateResult updateResult = gradesCollection.updateOne(filtro, update);
			//Document updateResult = gradesCollection.findOneAndUpdate(filtro, update);
			//Document updateResult = gradesCollection.inser
			//resultado = (int) updateResult.getModifiedCount();
			//System.out.println(updateResult);
			//gradesCollection.insertOne(arrendador);
		} catch (Exception e) {
			System.out.println("Error al crear vehiculos: "+e.getMessage());
		}
	}

	@Override
	public int borrarVehiculo(Bson filtro, String filtroMatricula,String tipo) {
		ConexionMongo con = new ConexionMongo();
		int resultado = 0;
		MongoCollection<Document> gradesCollection;
		try {
			if (tipo.equals("arrendador")) {
				gradesCollection = con.crearConexion().getCollection("Arrendador");
			}else {
				gradesCollection = con.crearConexion().getCollection("Arrendatario");
			}
			
			
			gradesCollection.updateOne(filtro, Updates.pull("vehiculos", new Document("matricula",filtroMatricula)));
			
			//System.out.println(borrarVehiculo);
			/*Bson filtroGeneral = and(eq("rfc",filtroPersona),eq(""));
			
			DeleteResult result = gradesCollection.deleteOne(filtroGeneral);
			
			resultado= (int) result.getDeletedCount();*/
		} catch (Exception e) {
			System.out.println("Error mientras se borraba carro en dao");
			resultado=0;
		}
			
		return resultado;
	}

	@Override
	public int actualizarVehiculo(Vehiculo vehiculo, Bson filtro, Bson filtroMatricula,String tipo) {
		ConexionMongo con = new ConexionMongo();
		int resultado = 0;
		MongoCollection<Document> gradesCollection;
		
		try {
			if (tipo.equals("arrendador")) {
				System.out.println("daoVehiculo entra a arrendador");
				gradesCollection = con.crearConexion().getCollection("Arrendador");
			}else {
				System.out.println("daoVehiculo entra a arrendatario");
				gradesCollection = con.crearConexion().getCollection("Arrendatario");
			}
			Bson filter = and(filtro,filtroMatricula);
			
			Bson updateTipo = set("vehiculos.$.tipo", vehiculo.getTipo());
			Bson updateMarca = set("vehiculos.$.marca", vehiculo.getMarca());
			
			Bson updates = combine(updateTipo,updateMarca);
			
			UpdateResult updateResult = gradesCollection.updateOne(filter, updates);
			resultado = (int) updateResult.getModifiedCount();
		} catch (Exception e) {
			System.out.println("Error al actualizar vehiculo: "+e.getMessage());
			resultado = 0;
		}
		
		return resultado;
	}

}