package config;


import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

public class ConexionMongo {

	ConnectionString connectionString;
	MongoClientSettings settings;
	MongoClient mongoClient;
	
	public MongoDatabase crearConexion() throws Exception{
			connectionString = new ConnectionString(
					"mongodb+srv://root:root@cluster0.au2ct.mongodb.net/Residencia?retryWrites=true&w=majority");
			settings = MongoClientSettings.builder().applyConnectionString(connectionString)
					.build();
			mongoClient = MongoClients.create(settings);
			
			MongoDatabase sampleTrainingDB = mongoClient.getDatabase("Residencia");
			
			return sampleTrainingDB;
	}
}
