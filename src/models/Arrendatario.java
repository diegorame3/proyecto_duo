package models;

import java.time.LocalDate;

public class Arrendatario {

	 private ID id;
	    private String apellidoMat;
	    private String apellidoPat;
	    private Aval[] aval;
	    private String curp;
	    private String edad;
	    private String fechaContrato;
	    private String formaPago;
	    private String nombre;
	    private String numeroTelefono;
	    private String rfc;
	    private String numeroReferenciaPropiedad;
	    private Vehiculo[] vehiculos;

	    public ID getID() { return id; }
	    public void setID(ID value) { this.id = value; }

	    public String getApellidoMat() { return apellidoMat; }
	    public void setApellidoMat(String value) { this.apellidoMat = value; }

	    public String getApellidoPat() { return apellidoPat; }
	    public void setApellidoPat(String value) { this.apellidoPat = value; }

	    public Aval[] getAval() { return aval; }
	    public void setAval(Aval[] value) { this.aval = value; }

	    public String getCurp() { return curp; }
	    public void setCurp(String value) { this.curp = value; }

	    public String getEdad() { return edad; }
	    public void setEdad(String value) { this.edad = value; }

	    public String getFechaContrato() { return fechaContrato; }
	    public void setFechaContrato(String value) { this.fechaContrato = value; }

	    public String getFormaPago() { return formaPago; }
	    public void setFormaPago(String value) { this.formaPago = value; }

	    public String getNombre() { return nombre; }
	    public void setNombre(String value) { this.nombre = value; }

	    public String getNumeroTelefono() { return numeroTelefono; }
	    public void setNumeroTelefono(String value) { this.numeroTelefono = value; }

	    public String getRFC() { return rfc; }
	    public void setRFC(String value) { this.rfc = value; }

	    public String getNumeroReferenciaPropiedad() { return numeroReferenciaPropiedad; }
	    public void setNumeroReferenciaPropiedad(String value) { this.numeroReferenciaPropiedad = value; }
	    
	    public Vehiculo[] getVehiculos() { return vehiculos; }
	    public void setVehiculos(Vehiculo[] value) { this.vehiculos = value; }
}
