package models;

public class Vehiculo {
    private String matricula;
    private String tipo;
    private String marca;

    public String getMatricula() { return matricula; }
    public void setMatricula(String value) { this.matricula = value; }

    public String getTipo() { return tipo; }
    public void setTipo(String value) { this.tipo = value; }

    public String getMarca() { return marca; }
    public void setMarca(String value) { this.marca = value; }
}
