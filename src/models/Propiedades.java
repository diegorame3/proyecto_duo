package models;

public class Propiedades {
	
	  private Direccion direccion;
	    private boolean estatusRenta;
	    private String descripcion;
	    private String numeroReferencia;
	    private Entrada[] entradas;

	    public Direccion getDireccion() { return direccion; }
	    public void setDireccion(Direccion value) { this.direccion = value; }

	    public boolean getEstatusRenta() { return estatusRenta; }
	    public void setEstatusRenta(boolean value) { this.estatusRenta = value; }

	    public String getDescripcion() { return descripcion; }
	    public void setDescripcion(String value) { this.descripcion = value; }

	    public String getNumeroReferencia() { return numeroReferencia; }
	    public void setNumeroReferencia(String value) { this.numeroReferencia = value; }

	    public Entrada[] getEntradas() { return entradas; }
	    public void setEntradas(Entrada[] value) { this.entradas = value; }

}
