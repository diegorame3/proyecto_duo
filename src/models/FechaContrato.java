package models;

import java.time.OffsetDateTime;

public class FechaContrato {
    private OffsetDateTime date;

    public OffsetDateTime getDate() { return date; }
    public void setDate(OffsetDateTime value) { this.date = value; }
}