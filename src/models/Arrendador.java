package models;

public class Arrendador {
	
	  	private ID id;
	    private String apellidoMat;
	    private String apellidoPat;
	    private String curp;
	    private long edad;
	    private String nombre;
	    private String numeroTelefonico;
	    private Propiedades[] propiedades;
	    private String rfc;
	    private Vehiculo[] vehiculos;

	    public ID getID() { return id; }
	    public void setID(ID value) { this.id = value; }

	    public String getApellidoMat() { return apellidoMat; }
	    public void setApellidoMat(String value) { this.apellidoMat = value; }

	    public String getApellidoPat() { return apellidoPat; }
	    public void setApellidoPat(String value) { this.apellidoPat = value; }

	    public String getCurp() { return curp; }
	    public void setCurp(String value) { this.curp = value; }

	    public long getEdad() { return edad; }
	    public void setEdad(long value) { this.edad = value; }

	    public String getNombre() { return nombre; }
	    public void setNombre(String value) { this.nombre = value; }

	    public String getNumeroTelefonico() { return numeroTelefonico; }
	    public void setNumeroTelefonico(String value) { this.numeroTelefonico = value; }

	    public Propiedades[] getPropiedades() { return propiedades; }
	    public void setPropiedades(Propiedades[] value) { this.propiedades = value; }

	    public String getRFC() { return rfc; }
	    public void setRFC(String value) { this.rfc = value; }

	    public Vehiculo[] getVehiculos() { return vehiculos; }
	    public void setVehiculos(Vehiculo[] value) { this.vehiculos = value; }
}
