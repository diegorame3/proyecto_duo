package models;

public class Administrador {
    private ID id;
    private String alias;
    private String correo;
    private String password;

    public ID getID() { return id; }
    public void setID(ID value) { this.id = value; }

    public String getAlias() { return alias; }
    public void setAlias(String value) { this.alias = value; }

    public String getCorreo() { return correo; }
    public void setCorreo(String value) { this.correo = value; }

    public String getPassword() { return password; }
    public void setPassword(String value) { this.password = value; }
}
