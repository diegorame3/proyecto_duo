package models;

public class Direccion {
    private String calle;
    private String numero;
    private String zona;
    private String cp;

    public String getCalle() { return calle; }
    public void setCalle(String value) { this.calle = value; }

    public String getNumero() { return numero; }
    public void setNumero(String value) { this.numero = value; }

    public String getZona() { return zona; }
    public void setZona(String value) { this.zona = value; }

    public String getCp() { return cp; }
    public void setCp(String value) { this.cp = value; }
}
