package models;

public class Aval {
    private String nombre;
    private String apellidoPat;
    private String apellidoMat;
    private String rfc;
    private String curp;
    private long edad;
    private String numeroTelefonico;
    private String direccion;
    private String tipoAval;

    public String getNombre() { return nombre; }
    public void setNombre(String value) { this.nombre = value; }

    public String getApellidoPat() { return apellidoPat; }
    public void setApellidoPat(String value) { this.apellidoPat = value; }

    public String getApellidoMat() { return apellidoMat; }
    public void setApellidoMat(String value) { this.apellidoMat = value; }

    public String getRFC() { return rfc; }
    public void setRFC(String value) { this.rfc = value; }

    public String getCurp() { return curp; }
    public void setCurp(String value) { this.curp = value; }

    public long getEdad() { return edad; }
    public void setEdad(long value) { this.edad = value; }

    public String getNumeroTelefonico() { return numeroTelefonico; }
    public void setNumeroTelefonico(String value) { this.numeroTelefonico = value; }

    public String getDireccion() { return direccion; }
    public void setDireccion(String value) { this.direccion = value; }

    public String getTipoAval() { return tipoAval; }
    public void setTipoAval(String value) { this.tipoAval = value; }
}
